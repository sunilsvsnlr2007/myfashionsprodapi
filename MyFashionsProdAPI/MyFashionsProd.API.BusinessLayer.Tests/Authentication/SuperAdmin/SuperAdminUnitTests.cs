﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyFashionsProd.API.BusinessLayer.Authentication;
using MyFashionsProd.API.ClientSDK.Super_Admin_Calls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Tests.Authentication.SuperAdmin
{
    [TestClass]
    public class SuperAdminUnitTests
    {
        [TestMethod]
        public void ValidateSuperAdminWithAPI()
        {
            SuperAdminCalls super = new SuperAdminCalls();
            string result = super.ValidateSuperAdmin();
            Assert.AreEqual("Success", result);
        }

        [TestMethod]
        public void ValidateSuperAdmin()
        {
            long superAdminID = 0;
            SuperAdminLogic superAdminLogic = new SuperAdminLogic();
            string result = superAdminLogic.ValidateSuperAdmin("ADMIN", "MYFASHIONS", out superAdminID);
            Assert.AreEqual(Messages.Succcess, result);
        }

        [TestMethod]
        public void InvalidSuperAdminUserName()
        {
            long superAdminID = 0;
            SuperAdminLogic superAdminLogic = new SuperAdminLogic();
            string result = superAdminLogic.ValidateSuperAdmin("", "MYFASHIONS", out superAdminID);
            Assert.AreEqual(Messages.InvalidUserName, result);
        }

        [TestMethod]
        public void InvalidSuperAdminPassword()
        {
            long superAdminID = 0;
            SuperAdminLogic superAdminLogic = new SuperAdminLogic();
            string result = superAdminLogic.ValidateSuperAdmin("ADMIN", "", out superAdminID);
            Assert.AreEqual(Messages.InvalidPassword, result);
        }
    }
}
