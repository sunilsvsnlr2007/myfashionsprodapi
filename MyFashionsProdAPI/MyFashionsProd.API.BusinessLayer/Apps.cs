﻿using MyfashionsDB.DBEnums;
using MyfashionsDB.DBFolder;
using MyfashionsDB.InitiateSeed;
using MyfashionsDB.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer
{
    public class Apps
    {
        public static MyfashionsDBContext dbContext = new MyfashionsDBContext();
        public static Logger Logger = LogManager.GetLogger("MyFashionsProductAPI");
        public static bool InitiateSeed()
        {
            //dbContext.Database.Log = s => Logger.Info(s); //To Log EF generated queries. Please uncomment
            //RunDatabaseSeed seed = new RunDatabaseSeed();
            //return seed.RunSeed(dbContext);

            try
            {
                if (dbContext.SuperAdmin.ToList().Count.Equals(0))
                {
                    dbContext.SuperAdmin.Add(new SuperAdmin()
                    {
                        ID = 1,
                        USERNAME = "ADMIN",
                        PASSWORD = Security.EncryptAndDecrypt.Encrypt("MYFASHIONS"),
                        VERSION = 1,
                        LASTUPDATEDTIME = DateTime.Now
                    });
                }
                dbContext.SaveChanges();

                int i = 1;
                foreach (string item in Enum.GetNames(typeof(TopEnums)))
                {
                    if (!dbContext.MainEnum.ToList().Exists(c => c.NAME.Equals(item)))
                    {
                        dbContext.MainEnum.Add(new MainEnum()
                        {
                            ID = i++,
                            NAME = item,
                            ISACTIVE = true,
                            LASTUPDATEDTIME = DateTime.Now,
                            VERSION = 1,
                        });
                    }
                }
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
