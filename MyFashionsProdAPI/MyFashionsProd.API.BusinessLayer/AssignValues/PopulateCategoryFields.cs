﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.AssignValues
{
    public class PopulateCategoryFields
    {
        public static MasterCategories AssignSubCategoryValues(MasterCategories masterCategObj, MasterCategories source)
        {
            if (masterCategObj == null)
                masterCategObj = new MasterCategories();

            if (source != null)
            {
                masterCategObj.ID = source.ID;
                masterCategObj.IMAGELOCATION = source.IMAGELOCATION;
                masterCategObj.IMAGENAME = source.IMAGENAME;
                masterCategObj.ISACTIVE = source.ISACTIVE;
                masterCategObj.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                masterCategObj.NAME = source.NAME;
                masterCategObj.SNO = source.SNO;
                masterCategObj.VERSION = source.VERSION;
                masterCategObj.WEBIMAGELOCATION = source.WEBIMAGELOCATION;
                masterCategObj.REGCOMPANY = PopulateCompanyFields.AssignCompanyValues(masterCategObj.REGCOMPANY, source.REGCOMPANY);
                if (source.SELECTEDENUM != null && source.SELECTEDENUM.ID > 0)
                    masterCategObj.SELECTEDENUM = PopulateEnumFields.AssignIndividualEnumValues(masterCategObj.SELECTEDENUM, source.SELECTEDENUM);
                if (source.SELECTEDCATEG != null && source.SELECTEDCATEG.ID > 0)
                    masterCategObj.SELECTEDCATEG = AssignIndividualSubCategoryValues(masterCategObj.SELECTEDCATEG, source.SELECTEDCATEG);
                if (source.REGCOMPANY != null && source.REGCOMPANY.ID > 0)
                    masterCategObj.REGCOMPANY = PopulateCompanyFields.AssignCompanyValues(masterCategObj.REGCOMPANY, source.REGCOMPANY);

                if (source.LSTSUBCATEGORIES != null)
                {
                    if (masterCategObj.LSTSUBCATEGORIES == null)
                        masterCategObj.LSTSUBCATEGORIES = new List<MasterCategories>();
                    
                    source.LSTSUBCATEGORIES.ToList().ForEach(c =>
                    {
                        MasterCategories mainSubCategory = new MasterCategories();
                        mainSubCategory = AssignSubCategoryValues(mainSubCategory, c);
                        masterCategObj.LSTSUBCATEGORIES.Add(mainSubCategory);
                    });
                }
            }
            return masterCategObj;
        }

        private static MasterCategories AssignIndividualSubCategoryValues(MasterCategories masterCategObj, MasterCategories source)
        {
            if (masterCategObj == null)
                masterCategObj = new MasterCategories();

            if (source != null)
            {
                masterCategObj.ID = source.ID;
                masterCategObj.IMAGELOCATION = source.IMAGELOCATION;
                masterCategObj.IMAGENAME = source.IMAGENAME;
                masterCategObj.ISACTIVE = source.ISACTIVE;
                masterCategObj.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                masterCategObj.NAME = source.NAME;
                masterCategObj.SNO = source.SNO;
                masterCategObj.VERSION = source.VERSION;
                masterCategObj.WEBIMAGELOCATION = source.WEBIMAGELOCATION;
                masterCategObj.REGCOMPANY = PopulateCompanyFields.AssignCompanyValues(masterCategObj.REGCOMPANY, source.REGCOMPANY);

            }
            return masterCategObj;
        }

        public static MasterCategories AssignCreateMasterCategoriesValues(MasterCategories masterCategObj, MasterCategories source)
        {
            if (masterCategObj == null)
                masterCategObj = new MasterCategories();

            if (source != null)
            {
                masterCategObj.ID = source.ID;
                masterCategObj.IMAGELOCATION = source.IMAGELOCATION;
                masterCategObj.IMAGENAME = source.IMAGENAME;
                masterCategObj.ISACTIVE = source.ISACTIVE;
                masterCategObj.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                masterCategObj.NAME = source.NAME;
                masterCategObj.SNO = source.SNO;
                masterCategObj.VERSION = source.VERSION;
                masterCategObj.WEBIMAGELOCATION = source.WEBIMAGELOCATION;
            }
            return masterCategObj;
        }
    }
}
