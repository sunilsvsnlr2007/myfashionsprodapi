﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.AssignValues
{
    public class PopulateCompleteLookFields
    {
        public static CompleteLook AssignCompleteLookValues(CompleteLook completeLook, CompleteLook source)
        {
            if (completeLook == null)
                completeLook = new CompleteLook();
            if (source != null)
            {
                completeLook.ID = source.ID;
                completeLook.IMAGELOCATION = source.IMAGELOCATION;
                completeLook.IMAGENAME = source.IMAGENAME;
                completeLook.ISJEWELLERY = source.ISJEWELLERY;
                completeLook.ITEMCODE = source.ITEMCODE;
                completeLook.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                completeLook.NAME = source.NAME;
                completeLook.PRODUCTITEMID = source.PRODUCTITEMID;
                completeLook.PRODUCTITEMSNO = source.PRODUCTITEMSNO;
                completeLook.SNO = source.SNO;
                completeLook.TEMPIMAGELOCATION = source.TEMPIMAGELOCATION;
                completeLook.VERSION = source.VERSION;
                completeLook.WEBIMAGELOCATION = source.WEBIMAGELOCATION;
            }
            return completeLook;
        }
    }
}
