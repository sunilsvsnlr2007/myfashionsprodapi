﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.AssignValues
{
    public class PopulateEnumFields
    {
        public static MainEnum AssignIndividualEnumValues(MainEnum mainMasterEnum, MainEnum source)
        {
            if (mainMasterEnum == null)
                mainMasterEnum = new MainEnum();
            if (source != null)
            {
                mainMasterEnum.ID = source.ID;
                mainMasterEnum.IMAGELOCATION = source.IMAGELOCATION;
                mainMasterEnum.IMAGENAME = source.IMAGENAME;
                mainMasterEnum.ISACTIVE = source.ISACTIVE;
                mainMasterEnum.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                mainMasterEnum.NAME = source.NAME;
                mainMasterEnum.SNO = source.SNO;
                mainMasterEnum.VERSION = source.VERSION;
                mainMasterEnum.WEBIMAGELOCATION = source.WEBIMAGELOCATION;
            }
            return mainMasterEnum;
        }
    }
}
