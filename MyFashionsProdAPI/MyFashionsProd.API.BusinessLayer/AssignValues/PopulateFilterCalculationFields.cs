﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.AssignValues
{
    public class PopulateFilterCalculationFields
    {
        public static FilterCalculation AssignFilterCalculationFields(FilterCalculation filterCal, FilterCalculation source)
        {
            if (filterCal == null)
                filterCal = new FilterCalculation();
            if (source != null)
            {
                filterCal.ID = source.ID;
                filterCal.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                filterCal.MASTERFILTER = PopulateFiltersFields.AssignOnlyFilterFields(filterCal.MASTERFILTER, source.MASTERFILTER);
                filterCal.SNO = source.SNO;
                filterCal.VERSION = source.VERSION;
                filterCal.WEIGHT = source.WEIGHT;
            }
            return filterCal;
        }
    }
}
