﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.AssignValues
{
    public class PopulateFiltersFields
    {
        public static MasterFilters AssignFilterValues(MasterFilters masterFilters, MasterFilters source)
        {
            if (masterFilters == null)
                masterFilters = new MasterFilters();
            if (source != null)
            {
                masterFilters.ID = source.ID;
                masterFilters.IMAGELOCATION = source.IMAGELOCATION;
                masterFilters.IMAGENAME = source.IMAGENAME;
                masterFilters.ISACTIVE = source.ISACTIVE;
                masterFilters.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                masterFilters.NAME = source.NAME;
                masterFilters.VERSION = source.VERSION;
                masterFilters.SNO = source.SNO;
                masterFilters.WEBIMAGELOCATION = source.WEBIMAGELOCATION;
                
                if (source.SELECTEDFILTER != null)
                {
                    masterFilters.SELECTEDFILTER = AssignOnlyFilterFields(masterFilters.SELECTEDFILTER, source.SELECTEDFILTER);
                }

                if (source.SELECTEDENUM != null)
                {
                    masterFilters.SELECTEDENUM = PopulateEnumFields.AssignIndividualEnumValues(masterFilters.SELECTEDENUM, source.SELECTEDENUM);
                }

                if (source.REGCOMPANY != null)
                {
                    masterFilters.REGCOMPANY = PopulateCompanyFields.AssignCompanyValues(masterFilters.REGCOMPANY, source.REGCOMPANY);
                }

                if (source.MARKETPRICE != null)
                {
                    masterFilters.MARKETPRICE = PopulateMarketPriceFields.AssignMarketPriceValues(masterFilters.MARKETPRICE, source.MARKETPRICE);
                }

                if (source.LSTREGSTORE != null && source.LSTREGSTORE.Count > 0)
                {
                    if (masterFilters.LSTREGSTORE == null)
                        masterFilters.LSTREGSTORE = new List<Stores>();

                    source.LSTREGSTORE.ToList().ForEach(d =>
                    {
                        Stores store = new Stores();
                        store = PopulateStoreFields.AssignStoreValues(store, d);
                        masterFilters.LSTREGSTORE.Add(store);
                    });
                }

                if (source.LSTAPPLIEDSUBCATEGORIES != null && source.LSTAPPLIEDSUBCATEGORIES.Count > 0)
                {
                    if (masterFilters.LSTAPPLIEDSUBCATEGORIES == null)
                        masterFilters.LSTAPPLIEDSUBCATEGORIES = new List<MasterCategories>();

                    source.LSTAPPLIEDSUBCATEGORIES.ToList().ForEach(d =>
                    {
                        MasterCategories masterCateg = new MasterCategories();
                        masterCateg = PopulateCategoryFields.AssignCreateMasterCategoriesValues(masterCateg, d);
                        source.LSTAPPLIEDSUBCATEGORIES.Add(masterCateg);
                    });
                }

                if (source.LSTSUBFILTERS != null && source.LSTSUBFILTERS.Count > 0)
                {
                    if (masterFilters.LSTSUBFILTERS == null)
                        masterFilters.LSTSUBFILTERS = new List<MasterFilters>();

                    source.LSTSUBFILTERS.ToList().ForEach(d =>
                    {
                        MasterFilters filter = new MasterFilters();
                        filter = AssignFilterValues(filter, d);
                        masterFilters.LSTSUBFILTERS.Add(filter);
                    });
                }
            }
            return masterFilters;
        }

        public static MasterFilters AssignFiltersBasedOnStore(Stores storeObj, MasterFilters masterFilters, MasterFilters source)
        {
            if (masterFilters == null)
                masterFilters = new MasterFilters();
            if (source != null && storeObj != null)
            {
                masterFilters.ID = source.ID;
                masterFilters.IMAGELOCATION = source.IMAGELOCATION;
                masterFilters.IMAGENAME = source.IMAGENAME;
                masterFilters.ISACTIVE = source.ISACTIVE;
                masterFilters.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                masterFilters.NAME = source.NAME;
                masterFilters.VERSION = source.VERSION;
                masterFilters.SNO = source.SNO;
                masterFilters.WEBIMAGELOCATION = source.WEBIMAGELOCATION;

                if (source.SELECTEDFILTER != null)
                {
                    masterFilters.SELECTEDFILTER = AssignOnlyFilterFields(masterFilters.SELECTEDFILTER, source.SELECTEDFILTER);
                }

                if (source.SELECTEDENUM != null)
                {
                    masterFilters.SELECTEDENUM = PopulateEnumFields.AssignIndividualEnumValues(masterFilters.SELECTEDENUM, source.SELECTEDENUM);
                }

                if (source.REGCOMPANY != null)
                {
                    masterFilters.REGCOMPANY = PopulateCompanyFields.AssignCompanyValues(masterFilters.REGCOMPANY, source.REGCOMPANY);
                }

                if (source.MARKETPRICE != null)
                {
                    masterFilters.MARKETPRICE = PopulateMarketPriceFields.AssignMarketPriceValues(masterFilters.MARKETPRICE, source.MARKETPRICE);
                }

                if (source.LSTSUBFILTERS != null && source.LSTSUBFILTERS.Count > 0)
                {
                    if (masterFilters.LSTSUBFILTERS == null)
                        masterFilters.LSTSUBFILTERS = new List<MasterFilters>();

                    source.LSTSUBFILTERS.Where(c => c.LSTREGSTORE != null && c.LSTREGSTORE.Equals(storeObj)).ToList().ForEach(c =>
                    {
                        MasterFilters filter = new MasterFilters();
                        filter = AssignFilterValues(filter, c);
                        masterFilters.LSTSUBFILTERS.Add(filter);
                    });
                }

                if (source.LSTAPPLIEDSUBCATEGORIES != null && source.LSTAPPLIEDSUBCATEGORIES.Count > 0)
                {
                    if (masterFilters.LSTAPPLIEDSUBCATEGORIES == null)
                        masterFilters.LSTAPPLIEDSUBCATEGORIES = new List<MasterCategories>();

                    source.LSTAPPLIEDSUBCATEGORIES.ToList().ForEach(c =>
                    {
                        MasterCategories masterCateg = new MasterCategories();
                        masterCateg = PopulateCategoryFields.AssignCreateMasterCategoriesValues(masterCateg, c);
                        source.LSTAPPLIEDSUBCATEGORIES.Add(masterCateg);
                    });
                }
            }
            return masterFilters;
        }

        public static MasterFilters AssignOnlyFilterFields(MasterFilters masterFilter,MasterFilters source)
        {
            if (masterFilter == null)
                masterFilter = new MasterFilters();
            if (source != null)
            {
                masterFilter.ID = source.ID;
                masterFilter.IMAGELOCATION = source.IMAGELOCATION;
                masterFilter.IMAGENAME = source.IMAGENAME;
                masterFilter.ISACTIVE = source.ISACTIVE;
                masterFilter.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                masterFilter.NAME = source.NAME;
                masterFilter.VERSION = source.VERSION;
                masterFilter.SNO = source.SNO;
                masterFilter.WEBIMAGELOCATION = source.WEBIMAGELOCATION;

                if (masterFilter.LSTAPPLIEDSUBCATEGORIES == null)
                    masterFilter.LSTAPPLIEDSUBCATEGORIES = new List<MasterCategories>();
                if (masterFilter.LSTPRODUCTS == null)
                    masterFilter.LSTPRODUCTS = new List<Product>();
                if (masterFilter.LSTREGSTORE == null)
                    masterFilter.LSTREGSTORE = new List<Stores>();
                if (masterFilter.LSTSUBFILTERS == null)
                    masterFilter.LSTSUBFILTERS = new List<MasterFilters>();
                if (masterFilter.REGCOMPANY == null)
                    masterFilter.REGCOMPANY = new Company();
            }
            return masterFilter;
        }
    }
}
