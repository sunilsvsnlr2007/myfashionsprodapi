﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.AssignValues
{
    public class PopulateMarketPriceFields
    {
        public static MarketPrice AssignMarketPriceValues(MarketPrice marketPrice, MarketPrice source)
        {
            if (marketPrice == null)
            {
                marketPrice = new MarketPrice();
            }

            if (source != null)
            {
                marketPrice.ID = source.ID;
                marketPrice.IDNUM = source.IDNUM;
                marketPrice.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                marketPrice.NOTATION = source.NOTATION;
                marketPrice.NOTES = source.NOTES;
                marketPrice.PRICE = source.PRICE;
                marketPrice.VERSION = source.VERSION;
            }
            return marketPrice;
        }
    }
}
