﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.AssignValues
{
    public class PopulateOfferFields
    {
        public static MasterOffers AssignMasterOfferValues(MasterOffers masterOfferObj, MasterOffers source)
        {
            if (masterOfferObj == null)
                masterOfferObj = new MasterOffers();

            if (source != null)
            {
                masterOfferObj.ID = source.ID;
                masterOfferObj.NAME = source.NAME;
                masterOfferObj.BUYVALUE = source.BUYVALUE;
                masterOfferObj.DESCRIPTION = source.DESCRIPTION;
                masterOfferObj.DISCOUNT = source.DISCOUNT;
                masterOfferObj.DISCOUNTTYPE = source.DISCOUNTTYPE;
                masterOfferObj.GETVALUE = source.GETVALUE;
                masterOfferObj.IMAGELOCATION = source.IMAGELOCATION;
                masterOfferObj.IMAGENAME = source.IMAGENAME;
                masterOfferObj.ISEXPIRED = source.ISEXPIRED;
                masterOfferObj.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                masterOfferObj.SNO = source.SNO;
                masterOfferObj.TEMPIMAGELOCATION = source.TEMPIMAGELOCATION;
                masterOfferObj.VERSION = source.VERSION;
                masterOfferObj.WEBIMAGELOCATION = source.WEBIMAGELOCATION;
                
                masterOfferObj.REGCOMPANY = PopulateCompanyFields.AssignCompanyValues(masterOfferObj.REGCOMPANY, source.REGCOMPANY);

                if (source.SELECTEDOFFER != null)
                {
                    masterOfferObj.SELECTEDOFFER = AssignFieldsForCreateOffer(masterOfferObj.SELECTEDOFFER, source.SELECTEDOFFER);
                }

                if (source.SELECTEDENUM != null)
                {
                    masterOfferObj.SELECTEDENUM = PopulateEnumFields.AssignIndividualEnumValues(masterOfferObj.SELECTEDENUM, source.SELECTEDENUM);
                }

                if (source.LSTREGSTORE != null)
                {
                    List<Stores> lstStores = new List<Stores>();
                    source.LSTREGSTORE.ForEach(d =>
                    {
                        Stores store = new Stores();
                        store = PopulateStoreFields.AssignStoreValues(store, d);
                        lstStores.Add(store);
                    });
                    masterOfferObj.LSTREGSTORE = lstStores;
                }

                if (source.LSTSUBOFFERS != null)
                {
                    List<MasterOffers> lstSubOffers = new List<MasterOffers>();
                    source.LSTSUBOFFERS.ToList().ForEach(d =>
                    {
                        MasterOffers masterOffer = new MasterOffers();
                        masterOffer = AssignMasterOfferValues(masterOffer, d);
                        lstSubOffers.Add(masterOffer);
                    });
                    masterOfferObj.LSTSUBOFFERS = lstSubOffers;
                }
            }
            return masterOfferObj;
        }

        public static MasterOffers AssignFieldsForCreateOffer(MasterOffers mainMasterOffer, MasterOffers source)
        {
            if (mainMasterOffer == null)
                mainMasterOffer = new MasterOffers();

            if (source != null)
            {
                mainMasterOffer.SNO = source.SNO;
                mainMasterOffer.BUYVALUE = source.BUYVALUE;
                mainMasterOffer.DESCRIPTION = source.DESCRIPTION;
                mainMasterOffer.DISCOUNT = source.DISCOUNT;
                mainMasterOffer.DISCOUNTTYPE = source.DISCOUNTTYPE;
                mainMasterOffer.GETVALUE = source.GETVALUE;
                mainMasterOffer.ID = source.ID;
                mainMasterOffer.IMAGELOCATION = source.IMAGELOCATION;
                mainMasterOffer.IMAGENAME = source.IMAGENAME;
                mainMasterOffer.ISEXPIRED = source.ISEXPIRED;
                mainMasterOffer.LASTUPDATEDTIME = DateTime.Now;
                mainMasterOffer.NAME = source.NAME;
                mainMasterOffer.TEMPIMAGELOCATION = source.TEMPIMAGELOCATION;
                mainMasterOffer.VERSION = source.VERSION;
                mainMasterOffer.WEBIMAGELOCATION = source.WEBIMAGELOCATION;

                if (mainMasterOffer.LSTAPPLIEDSUBCATEGORIES == null)
                    mainMasterOffer.LSTAPPLIEDSUBCATEGORIES = new List<MasterCategories>();
                if (mainMasterOffer.LSTPRODUCTS == null)
                    mainMasterOffer.LSTPRODUCTS = new List<Product>();
                if (mainMasterOffer.LSTREGSTORE == null)
                    mainMasterOffer.LSTREGSTORE = new List<Stores>();
                if (mainMasterOffer.LSTSUBOFFERS == null)
                    mainMasterOffer.LSTSUBOFFERS = new List<MasterOffers>();
                if (mainMasterOffer.REGCOMPANY == null)
                    mainMasterOffer.REGCOMPANY = new Company();
            }
            return mainMasterOffer;
        }
    }
}