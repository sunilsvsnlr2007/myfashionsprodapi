﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.AssignValues
{
    class PopulateSimilarProductFields
    {
        public static SimilarProduct AssignSimilarProductValues(SimilarProduct similarProd, SimilarProduct source)
        {
            if (similarProd == null)
                similarProd = new SimilarProduct();
            if (source != null)
            {
                similarProd.ID = source.ID;
                similarProd.IMAGELOCATION = source.IMAGELOCATION;
                similarProd.IMAGENAME = source.IMAGENAME;
                similarProd.ITEMCODE = source.ITEMCODE;
                similarProd.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                similarProd.NAME = source.NAME;
                similarProd.PRODUCTITEMID = source.PRODUCTITEMID;
                similarProd.PRODUCTITEMSNO = source.PRODUCTITEMSNO;
                similarProd.SNO = source.SNO;
                similarProd.TEMPIMAGELOCATION = source.TEMPIMAGELOCATION;
                similarProd.VERSION = source.VERSION;
                similarProd.WEBIMAGELOCATION = source.WEBIMAGELOCATION;
            }
            return similarProd;
        }
    }
}
