﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.AssignValues
{
    public class PopulateStoreFields
    {
        public static Stores AssignStoreValues(Stores mainStoreObj, Stores source)
        {
            if (mainStoreObj == null)
                mainStoreObj = new Stores();

            if (source != null)
            {
                mainStoreObj.ADDRES1 = source.ADDRES1;
                mainStoreObj.ADDRESS2 = source.ADDRESS2;
                mainStoreObj.CITY = source.CITY;
                mainStoreObj.STOREID = source.STOREID;
                mainStoreObj.ID = source.ID;
                mainStoreObj.IMAGELOCATION = source.IMAGELOCATION;
                mainStoreObj.IMAGENAME = source.IMAGENAME;
                mainStoreObj.ISACTIVE = source.ISACTIVE;
                mainStoreObj.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                mainStoreObj.NAME = source.NAME;
                mainStoreObj.NUMEMPLOYEES = source.NUMEMPLOYEES;
                mainStoreObj.PASSWORD = source.PASSWORD;
                mainStoreObj.STATE = source.STATE;
                mainStoreObj.USERNAME = source.USERNAME;
                mainStoreObj.VERSION = source.VERSION;
                mainStoreObj.WEBIMAGELOCATION = source.WEBIMAGELOCATION;
            }
            return mainStoreObj;
        }
    }
}
