﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.AssignValues
{
    public class PopulateSuperAdminFields
    {
        public static SuperAdmin AssignSuperAdminValues(SuperAdmin mainSuperAdmin, SuperAdmin source)
        {
            if (mainSuperAdmin == null)
                mainSuperAdmin = new SuperAdmin();

            if (source != null)
            {
                mainSuperAdmin.SNO = source.SNO;
                mainSuperAdmin.ID = source.ID;
                mainSuperAdmin.IMAGELOCATION = source.IMAGELOCATION;
                mainSuperAdmin.IMAGENAME = source.IMAGENAME;
                mainSuperAdmin.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                mainSuperAdmin.USERNAME = source.USERNAME;
                mainSuperAdmin.PASSWORD = source.PASSWORD;
                mainSuperAdmin.VERSION = source.VERSION;
                mainSuperAdmin.WEBIMAGELOCATION = source.WEBIMAGELOCATION;

            }
            return mainSuperAdmin;
        }
    }
}
