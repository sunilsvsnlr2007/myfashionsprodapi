﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using MyFashionsProd.API.BusinessLayer.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Authentication
{
    public class CompanyOperationsLogic
    {
        #region Validate Company Login
        public string ValidateCompanyLogin(string username, string password, out long companyID)
        {
            username = username.ToUpper();
            password = password.ToUpper();
            string encryptedPassword = EncryptAndDecrypt.Encrypt(password);

            Apps.Logger.Info("Validate Company Login: UserName: " + username + " Password: " + password + " Encrypted Password: " + encryptedPassword);
            string result = string.Empty;
            companyID = 0;
            Company validateCompany = Apps.dbContext.Company.FirstOrDefault(c => c.USERNAME.Trim().Equals(username) && c.PASSWORD.Trim().Equals(encryptedPassword) && c.ISACTIVE.Equals(true));
            if (validateCompany == null)
            {
                if (!Apps.dbContext.Company.ToList().Exists(c => c.USERNAME.Trim().Equals(username) && c.ISACTIVE.Equals(true)))
                    result = Messages.InvalidUserName;
                if (!Apps.dbContext.Company.ToList().Exists(c => c.PASSWORD.Trim().Equals(encryptedPassword) && c.ISACTIVE.Equals(true)))
                    result = Messages.InvalidPassword;
                else
                    result = Messages.UnKnown;
            }
            else
            {
                companyID = validateCompany.ID;
                result = Messages.Succcess;
            }
            Apps.Logger.Info("Result: " + result + " Company ID: " + companyID);
            return result;
        }
        #endregion

        #region Password Change
        public bool ChangePassword(string username, string oldpassword, string newpassword)
        {
            try
            {
                username = username.ToUpper();
                oldpassword = oldpassword.ToUpper();
                newpassword = newpassword.ToUpper();
                string encryptedOldPassword = EncryptAndDecrypt.Encrypt(oldpassword);
                string encryptedNewPassword = EncryptAndDecrypt.Encrypt(newpassword);
                Apps.Logger.Info("Intiated change password. UserName: " + username + " Old Password: " + oldpassword + " New Password: " + newpassword + " Encrypted Old Password: " + encryptedOldPassword + " Encrypted New Password: " + encryptedNewPassword);
                Company companyObj = Apps.dbContext.Company.Where(c => c.USERNAME.Equals(username) && c.PASSWORD.Equals(encryptedOldPassword) && c.ISACTIVE.Equals(true)).FirstOrDefault();
                if (companyObj != null)
                {
                    companyObj.PASSWORD = encryptedNewPassword;
                    companyObj.VERSION += 1;
                    companyObj.LASTUPDATEDTIME = DateTime.Now;
                    Apps.dbContext.SaveChanges();
                    Apps.Logger.Info(Messages.Succcess);
                    return true;
                }
                Apps.Logger.Info(Messages.Failed);
                return false;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while on ChangePassword: " + ex.Message, ex);
                return false;
            }
        }
        #endregion

        #region Fetch All Companies
        public List<Company> GetAllCompanies(long superAdminID)
        {
            Apps.Logger.Info("Initiated Get All Companies. SuperAdmin ID: " + superAdminID);
            List<Company> lstCompanies = new List<Company>();
            if (Apps.dbContext.Company.ToList().Count > 0)
            {
                Apps.dbContext.SuperAdmin.Where(c => c.ID.Equals(superAdminID)).ToList().ForEach(c =>
                {
                    //c.LSTCOMPANY.Where(d => d.ISACTIVE.Equals(true)).ToList().ForEach(d =>
                    c.LSTCOMPANY.ToList().ForEach(d =>
                    {
                        Company companyObj = new Company();
                        companyObj = PopulateCompanyFields.AssignCompanyValues(companyObj, d);
                        companyObj.SNO = d.SNO;
                        lstCompanies.Add(companyObj);
                    });
                });
            }
            Apps.Logger.Info(Messages.Succcess);
            return lstCompanies;
        }

        public Company GetCompanyInfo(long companyID, out string errorMsg)
        {
            Apps.Logger.Info("Initiated GetCompanyInfo: Company ID: " + companyID);
            errorMsg = string.Empty;
            if (companyID > 0)
            {
                Company comp = Apps.dbContext.Company.Where(c => c.ID.Equals(companyID)).FirstOrDefault();
                if (comp != null)
                {
                    Company companyObj = new Company();
                    companyObj = PopulateCompanyFields.AssignCompanyValues(companyObj, comp);
                    companyObj.SNO = comp.SNO;
                    Apps.Logger.Info(Messages.Succcess);
                    return companyObj;
                }
                else
                {
                    errorMsg = Messages.Company + " " + Messages.DoesnotExists;
                }
            }
            else
            {
                errorMsg = Messages.Company + " " + Messages.DoesnotExists;
            }
            Apps.Logger.Info("Error Message: " + errorMsg);
            return null;
        }
        #endregion

        #region Adding Company
        public string AddCompany(Company companyObj)
        {
            try
            {
                string result = string.Empty;
                Apps.Logger.Info("Initiated Add Company.");
                companyObj.USERNAME = companyObj.USERNAME.ToUpper();
                companyObj.PASSWORD = companyObj.PASSWORD.ToUpper();
                result = ValidateCompany(companyObj, "A");
                if (result.Equals(Messages.Succcess))
                {
                    Company mainCompanyObj = new Company();
                    mainCompanyObj = PopulateCompanyFields.AssignCompanyValues(mainCompanyObj, companyObj);
                    mainCompanyObj.ID = Apps.dbContext.Company.ToList().Count > 0 ? (Apps.dbContext.Company.Max(c => c.ID) + 1) : 1;
                    mainCompanyObj.REGISTEREDSUPERADMIN = Apps.dbContext.SuperAdmin.Where(c => c.ID.Equals(companyObj.REGISTEREDSUPERADMIN.ID)).FirstOrDefault();
                    mainCompanyObj.VERSION = 1;
                    mainCompanyObj.PASSWORD = EncryptAndDecrypt.Encrypt(mainCompanyObj.PASSWORD);

                    Apps.dbContext.Company.Add(mainCompanyObj);
                    Apps.dbContext.SaveChanges();
                    Apps.Logger.Info("Company ID: " + mainCompanyObj.ID);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while AddCompany: " + ex.Message, ex);
                return ex.Message;
            }
        }
        #endregion

        #region Updating Company
        public string UpdateCompany(Company companyObj)
        {
            try
            {
                string result = string.Empty;
                Apps.Logger.Info("Initiated Update Company.");
                result = ValidateCompany(companyObj, "U");
                if (result.Equals(Messages.Succcess))
                {
                    Company mainCompanyObj = Apps.dbContext.Company.FirstOrDefault(c => c.USERNAME.Equals(companyObj.USERNAME));
                    if (mainCompanyObj != null)
                    {
                        companyObj.USERNAME = mainCompanyObj.USERNAME;
                        companyObj.PASSWORD = mainCompanyObj.PASSWORD;
                        int currentVersion = mainCompanyObj.VERSION;
                        mainCompanyObj = PopulateCompanyFields.AssignCompanyValues(mainCompanyObj, companyObj);
                        mainCompanyObj.VERSION = currentVersion + 1;
                        Apps.dbContext.SaveChanges();
                        Apps.Logger.Info("Company ID: " + mainCompanyObj.ID);
                    }
                    else
                    {
                        result = Messages.UnableToFind + " " + Messages.Company;
                    }
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while updatecompany: " + ex.Message, ex);
                return ex.Message;
            }
        }
        #endregion

        #region Deleting Company
        public string DeleteCompany(Company companyObj)
        {
            try
            {
                string result = string.Empty;
                Apps.Logger.Info("Initiated Delete Company.");
                result = ValidateCompany(companyObj, "D");
                if (result.Equals(Messages.Succcess))
                {
                    Company mainCompanyObj = Apps.dbContext.Company.FirstOrDefault(c => c.ID.Equals(companyObj.ID) && c.ISACTIVE.Equals(true));
                    if (mainCompanyObj != null)
                    {
                        mainCompanyObj.ISACTIVE = false;
                        mainCompanyObj.VERSION += 1;
                        Apps.dbContext.SaveChanges();
                        Apps.Logger.Info("Company ID: " + mainCompanyObj.ID);
                    }
                    else
                    {
                        result = Messages.UnableToFind + " " + Messages.Company;
                    }
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while DeleteCompany: " + ex.Message, ex);
                return ex.Message;
            }
        }
        #endregion

        #region Validate Company Object
        private string ValidateCompany(Company companyObj, string code)
        {
            switch (code)
            {
                case "A":
                    #region Adding Validation

                    if (companyObj != null && !string.IsNullOrEmpty(companyObj.NAME) && !string.IsNullOrEmpty(companyObj.USERNAME) && !string.IsNullOrEmpty(companyObj.PASSWORD) && !string.IsNullOrEmpty(companyObj.IMAGELOCATION))
                    {
                        if (!Apps.dbContext.Company.ToList().Exists(c => c.NAME.Equals(companyObj.NAME) && c.ISACTIVE.Equals(true)))
                        {
                            if (!Apps.dbContext.Company.ToList().Exists(c => c.USERNAME.Equals(companyObj.USERNAME) && c.ISACTIVE.Equals(true)))
                            {
                                if (Apps.dbContext.SuperAdmin.ToList().Exists(c => c.ID.Equals(companyObj.REGISTEREDSUPERADMIN.ID) && c.USERNAME.Equals(companyObj.REGISTEREDSUPERADMIN.USERNAME)))
                                    return Messages.Succcess;
                                else
                                    return companyObj.REGISTEREDSUPERADMIN.USERNAME + " " + Messages.DoesnotExists;
                            }
                            else
                                return companyObj.USERNAME + " " + Messages.AlreadyExists;
                        }
                        else
                        {
                            return companyObj.NAME + " " + Messages.AlreadyExists;
                        }
                    }
                    else
                    {
                        return Messages.RequiredFieldsEmpty;
                    }

                    #endregion
                case "U":
                    #region Updation Validation

                    if (companyObj != null && !string.IsNullOrEmpty(companyObj.NAME) && !string.IsNullOrEmpty(companyObj.USERNAME) && !string.IsNullOrEmpty(companyObj.PASSWORD) && !string.IsNullOrEmpty(companyObj.IMAGELOCATION))
                    {
                        if (Apps.dbContext.Company.ToList().Exists(c => c.USERNAME.Equals(companyObj.USERNAME)))
                        {
                            return Messages.Succcess;
                        }
                        else
                        {
                            return companyObj.USERNAME + " " + Messages.DoesnotExists;
                        }
                    }
                    else
                    {
                        return Messages.RequiredFieldsEmpty;
                    }
                    #endregion
                case "D":
                    #region Deletion Validation
                    if (companyObj != null && Apps.dbContext.Company.ToList().Exists(c => c.USERNAME.Equals(companyObj.USERNAME) && c.ISACTIVE.Equals(true) && c.NAME.Equals(companyObj.NAME) && c.ID.Equals(companyObj.ID)))
                    {
                        return Messages.Succcess;
                    }
                    else
                    {
                        return Messages.InvalidObject;
                    }
                    #endregion
                default:
                    return Messages.UnKnown;
            }
        }
        #endregion
    }
}
