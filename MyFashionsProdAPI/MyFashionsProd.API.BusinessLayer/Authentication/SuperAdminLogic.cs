﻿using MyfashionsDB.InitiateSeed;
using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using MyFashionsProd.API.BusinessLayer.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Authentication
{
    public class SuperAdminLogic
    {
        static readonly object lockObject = new object();
        #region Validate Super Admin
        public string ValidateSuperAdmin(string username, string password, out long superAdminID)
        {
            lock (lockObject)
            {
                string result = string.Empty;
                superAdminID = 0;
                try
                {
                    Apps.Logger.Info("Initiated Validate Super Admin. UserName: " + username + " Password: " + password.ToUpper() + " Encrypted Password: " + EncryptAndDecrypt.Encrypt(password.ToUpper()));
                    string encryptedPassword = EncryptAndDecrypt.Encrypt(password.ToUpper());
                    SuperAdmin superAdmin = Apps.dbContext.SuperAdmin.FirstOrDefault(c => c.USERNAME.Equals(username) && c.PASSWORD.Equals(encryptedPassword));
                    if (superAdmin == null)
                    {
                        if (!Apps.dbContext.SuperAdmin.ToList().Exists(c => c.USERNAME.Equals(username)))
                        {
                            result = Messages.InvalidUserName;
                        }
                        else if (!Apps.dbContext.SuperAdmin.ToList().Exists(c => c.PASSWORD.Equals(encryptedPassword)))
                        {
                            result = Messages.InvalidPassword;
                        }
                        else
                        {
                            result = Messages.UnKnown;
                        }
                    }
                    else
                    {
                        superAdminID = superAdmin.ID;
                        result = Messages.Succcess;
                    }
                    Apps.Logger.Info("Super Admin ID: " + superAdminID + " Result: " + result);
                }
                catch (Exception ex)
                {
                    Apps.Logger.ErrorException("Exception while Validating Super Admin: " + ex.Message, ex);
                }
                return result;
            }
        }

        public SuperAdmin ValidateSuperAdmin(long superAdminID, out string errorMsg)
        {
            Apps.Logger.Info("Validating Super Admin By its ID: " + superAdminID);
            errorMsg = string.Empty;
            if (!Apps.dbContext.SuperAdmin.ToList().Exists(c => c.ID.Equals(superAdminID)))
            {
                errorMsg = Messages.SuperAdmin + " " + Messages.DoesnotExists;
            }
            else
            {
                SuperAdmin superAdmin = Apps.dbContext.SuperAdmin.Where(c => c.ID.Equals(superAdminID)).FirstOrDefault();
                SuperAdmin superObj = new SuperAdmin();
                superObj = PopulateSuperAdminFields.AssignSuperAdminValues(superObj, superAdmin);
                Apps.Logger.Info(Messages.Succcess);
                return superObj;
            }
            Apps.Logger.Info("Error Message: " + errorMsg);
            return null;
        }
        #endregion

        #region Change Super Admin Password
        public bool ChangePassword(string username, string oldpassword, string newpassword)
        {
            username = username.ToUpper();
            oldpassword = oldpassword.ToUpper();
            newpassword = newpassword.ToUpper();

            string encryptedOldPassword = EncryptAndDecrypt.Encrypt(oldpassword);
            string encryptedNewPassword = EncryptAndDecrypt.Encrypt(newpassword);

            Apps.Logger.Info("Changing Password: UserName: " + username + " Old Password: " + oldpassword + " New Password: " + newpassword + " Encrypted Old Password: " + encryptedOldPassword + " Encrypted New Password: " + encryptedNewPassword);
            SuperAdmin superObj = Apps.dbContext.SuperAdmin.FirstOrDefault(c => c.USERNAME.Equals(username) && c.PASSWORD.Equals(encryptedOldPassword));
            if (superObj != null)
            {
                superObj.PASSWORD = encryptedNewPassword;
                superObj.LASTUPDATEDTIME = DateTime.Now;
                superObj.VERSION += 1;
                Apps.dbContext.SaveChanges();
                Apps.Logger.Info("Password changed successfully.");
                return true;
            }
            return false;
        }
        #endregion

        #region Fetch Super Admins
        public List<SuperAdmin> GetAllSuperAdmins()
        {
            List<SuperAdmin> lstSuperAdmins = new List<SuperAdmin>();
            Apps.Logger.Info("Initiated Get All Super Admins");
            if (Apps.dbContext.SuperAdmin.ToList().Count > 0)
            {
                Apps.dbContext.SuperAdmin.ToList().ForEach(c =>
                {
                    SuperAdmin superObj = new SuperAdmin();
                    superObj = PopulateSuperAdminFields.AssignSuperAdminValues(superObj, c);
                    lstSuperAdmins.Add(superObj);
                });
            }
            Apps.Logger.Info(Messages.Succcess);
            return lstSuperAdmins;
        }
        #endregion

        #region Initiate Seed

        public bool InitiateSeed()
        {
            Apps.Logger.Info("Initiating Seed.");
            bool result = Apps.InitiateSeed();
            Apps.Logger.Info("Result: " + result);
            return result;
        }

        #endregion
    }
}
