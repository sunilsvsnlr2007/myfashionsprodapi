﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Complete_Look_Logic
{
    public class CRUDOperationsOnCompleteLook
    {
        internal static string AddOrUpdateCompleteLook(Product prod)
       { 
            string result = string.Empty;
            try
            {
                Product mainProd = Apps.dbContext.Product.Where(c => c.ID.Equals(prod.ID) && c.NAME.Equals(prod.NAME)).FirstOrDefault();
                if (mainProd.LSTCOMPLETELOOK == null)
                    mainProd.LSTCOMPLETELOOK = new List<CompleteLook>();

                #region Add or Update Complete Look
                prod.LSTCOMPLETELOOK.ToList().ForEach(c =>
                    {
                        if (!Apps.dbContext.CompleteLook.Any(d => d.ID.Equals(c.ID) && d.PRODUCTITEMID.Equals(c.PRODUCTITEMID)))
                        {
                            Product fetchedProd = Apps.dbContext.Product.Where(d => d.ID.Equals(c.PRODUCTITEMID)).FirstOrDefault();
                            CompleteLook completeLook = new CompleteLook(fetchedProd);
                            completeLook.ID = Apps.dbContext.CompleteLook.ToList().Count > 0 ? (Apps.dbContext.CompleteLook.Max(d => d.ID) + 1) : 1;
                            completeLook.VERSION = 1;

                            mainProd.LSTCOMPLETELOOK.Add(completeLook);
                        }
                    });

                mainProd.LSTCOMPLETELOOK.ToList().ForEach(c =>
                    {
                        if (!prod.LSTCOMPLETELOOK.Any(d => d.NAME.Equals(c.NAME) && d.PRODUCTITEMID.Equals(c.PRODUCTITEMID)))
                        {
                            mainProd.LSTCOMPLETELOOK.Remove(c);
                        }
                    });
                
                Apps.dbContext.SaveChanges();
                result = Messages.Succcess;
                #endregion
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while AddOrUpdateCompleteLook: " + ex.Message, ex);
                result = ex.Message;
            }
            Apps.Logger.Info("Result: " + result);
            return result;
        }

        internal static string UpdateCompleteLook(Product prod)
        {
            string result = string.Empty;
            Apps.Logger.Info("Updating Complete Look After product update.");

            try
            {
                if (Apps.dbContext.CompleteLook.Any(c => c.PRODUCTITEMID.Equals(prod.ID)))
                {
                    Apps.dbContext.CompleteLook.Where(c => c.PRODUCTITEMID.Equals(prod.ID)).ToList().ForEach(c =>
                    {
                        c.IMAGELOCATION = prod.IMAGELOCATION;
                        c.IMAGENAME = prod.IMAGENAME;
                        c.WEBIMAGELOCATION = prod.WEBIMAGELOCATION;
                        c.TEMPIMAGELOCATION = prod.TEMPIMAGELOCATION;
                        c.NAME = prod.NAME;
                        c.ITEMCODE = prod.ITEMCODE;
                        c.VERSION += 1;
                        c.LASTUPDATEDTIME = DateTime.Now;
                        Apps.dbContext.SaveChanges();
                    });
                    result = Messages.Succcess;
                }
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while UpdateCompleteLook: " + ex.Message, ex);
                result = ex.Message;
            }
            return result;
        }
    }
}
