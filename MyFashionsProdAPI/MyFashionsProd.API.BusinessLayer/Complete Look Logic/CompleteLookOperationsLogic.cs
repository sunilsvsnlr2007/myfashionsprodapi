﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Complete_Look_Logic
{
    public class CompleteLookOperationsLogic
    {
        public List<CompleteLook> GetAllCompleteLookUnderProduct(long productID, out string result)
        {
            Apps.Logger.Info("Initiating GetAllCompleteLookUnderProduct. Related Product ID: " + productID);
            result = string.Empty;
            List<CompleteLook> lstCompleteLook = new List<CompleteLook>();
            if (productID > 0)
            {
                Product prod = Apps.dbContext.Product.Where(c => c.ID.Equals(productID)).FirstOrDefault();
                if (prod != null)
                {
                    if (prod.LSTCOMPLETELOOK != null)
                    {
                        prod.LSTCOMPLETELOOK.ToList().ForEach(c =>
                            {
                                CompleteLook compLook = new CompleteLook();
                                compLook = PopulateCompleteLookFields.AssignCompleteLookValues(compLook, c);
                                lstCompleteLook.Add(compLook);
                            });
                    }
                    else
                    {
                        result = Messages.CompleteLook + " " + Messages.DoesnotExists;
                        Apps.Logger.Info(Messages.CompleteLook + " " + Messages.DoesnotExists);
                    }
                }
                else
                {
                    result = Messages.ProductMsg + " " + Messages.DoesnotExists;
                    Apps.Logger.Info(Messages.ProductMsg + " " + Messages.DoesnotExists);
                }
            }
            Apps.Logger.Info("Qualified Complete Look Count: " + lstCompleteLook.Count);
            if (string.IsNullOrEmpty(result))
                result = Messages.Succcess;

            return lstCompleteLook;
        }

        public string AddOrupdateCompleteLook(Product prod)
        {
            try
            {
                Apps.Logger.Info("Initiating AddOrupdateCompleteLook");
                string result = string.Empty;
                if (prod != null && prod.ID > 0 && !string.IsNullOrEmpty(prod.NAME))
                {
                    result = CRUDOperationsOnCompleteLook.AddOrUpdateCompleteLook(prod);
                }
                else
                {
                    result = Messages.ObjectShouldNotBeNull;
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while AddOrupdateCompleteLook: " + ex.Message, ex);
                return ex.Message;
            }
        }
    }
}
