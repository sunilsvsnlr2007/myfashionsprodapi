﻿using MyfashionsDB.DBEnums;
using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Filters
{
    public class CRUDOperationsOnFilters
    {
        internal static string AddMasterFilter(MasterFilters addFilter)
        {
            string result = string.Empty;
            if (addFilter != null)
            {
                try
                {
                    MasterFilters masterFilter = new MasterFilters();
                    masterFilter = PopulateFiltersFields.AssignOnlyFilterFields(masterFilter, addFilter);
                    masterFilter.ID = Apps.dbContext.MasterFilters.ToList().Count > 0 ? (Apps.dbContext.MasterFilters.Max(c => c.ID) + 1) : 1;
                    masterFilter.VERSION = 1;
                    masterFilter.REGCOMPANY = Apps.dbContext.Company.Where(c => c.ID.Equals(addFilter.REGCOMPANY.ID)).FirstOrDefault();

                    if (addFilter.SELECTEDFILTER != null && addFilter.SELECTEDFILTER.ID > 0 && Apps.dbContext.MasterFilters.Any(c => c.ID.Equals(addFilter.SELECTEDFILTER.ID)))
                    {
                        masterFilter.SELECTEDFILTER = Apps.dbContext.MasterFilters.FirstOrDefault(c => c.ID.Equals(addFilter.SELECTEDFILTER.ID));
                    }

                    if (addFilter.SELECTEDENUM != null)
                    {
                        masterFilter.SELECTEDENUM = Apps.dbContext.MainEnum.Where(c => c.ID.Equals(addFilter.SELECTEDENUM.ID) && c.ID.Equals((long)TopEnums.Filters)).FirstOrDefault();
                    }

                    if (addFilter.MARKETPRICE != null)
                    {
                        masterFilter.MARKETPRICE = Apps.dbContext.MarketPrice.Where(c => c.ID.Equals(addFilter.MARKETPRICE.ID)).FirstOrDefault();
                    }

                    if (addFilter.REGCOMPANY != null && addFilter.REGCOMPANY.ID > 0)
                    {
                        if (addFilter.LSTAPPLIEDSUBCATEGORIES != null && addFilter.LSTAPPLIEDSUBCATEGORIES.Count > 0)
                        {
                            addFilter.LSTAPPLIEDSUBCATEGORIES.Where(c => c.ISACTIVE.Equals(true)).ToList().ForEach(c =>
                                {
                                    MasterCategories fetchedCateg = Apps.dbContext.MasterCategories.Where(d => d.ID.Equals(c.ID)).FirstOrDefault();
                                    masterFilter.LSTAPPLIEDSUBCATEGORIES.Add(fetchedCateg);
                                });
                        }
                    }

                    if (addFilter.LSTREGSTORE != null && addFilter.LSTREGSTORE.Count > 0)
                    {
                        addFilter.LSTREGSTORE.Where(c => c.ISACTIVE.Equals(true)).ToList().ForEach(c =>
                        {
                            Stores fetchedStore = Apps.dbContext.Stores.Where(d => d.ID.Equals(c.ID)).FirstOrDefault();
                            masterFilter.LSTREGSTORE.Add(fetchedStore);
                        });
                    }

                    if (addFilter.LSTSUBFILTERS != null && addFilter.LSTSUBFILTERS.Count > 0)
                    {
                        addFilter.LSTSUBFILTERS.Where(c => c.ISACTIVE.Equals(true)).ToList().ForEach(c =>
                            {
                                MasterFilters fetchedFilter = Apps.dbContext.MasterFilters.Where(d => d.ID.Equals(c.ID)).FirstOrDefault();
                                masterFilter.LSTSUBFILTERS.Add(fetchedFilter);
                            });
                    }

                    Apps.dbContext.MasterFilters.Add(masterFilter);
                    Apps.dbContext.SaveChanges();
                    Apps.Logger.Info("Master Filter ID: " + masterFilter.ID);
                    result = Messages.Succcess;
                }
                catch (Exception ex)
                {
                    Apps.Logger.ErrorException("Exception while AddMasterFilter: " + ex.Message, ex);
                    result = ex.Message;
                }
            }
            else
            {
                result = Messages.ObjectShouldNotBeNull;
            }
            Apps.Logger.Info("Result: " + result);
            return result;
        }

        internal static string UpdateMasterFilter(MasterFilters updateFilter)
        {
            string result = string.Empty;
            if (updateFilter != null)
            {
                try
                {
                    int currentVersion = 0;
                    MasterFilters masterFilter = Apps.dbContext.MasterFilters.Where(c => c.ID.Equals(updateFilter.ID) && c.REGCOMPANY.ID.Equals(updateFilter.REGCOMPANY.ID)).FirstOrDefault();
                    if (masterFilter != null)
                    {
                        masterFilter = PopulateFiltersFields.AssignOnlyFilterFields(masterFilter, updateFilter);
                        masterFilter.VERSION = currentVersion + 1;
                        masterFilter.REGCOMPANY = Apps.dbContext.Company.Where(c => c.ID.Equals(updateFilter.REGCOMPANY.ID)).FirstOrDefault();

                        if (updateFilter.SELECTEDFILTER != null && updateFilter.SELECTEDFILTER.ID > 0 && Apps.dbContext.MasterFilters.Any(c => c.ID.Equals(updateFilter.SELECTEDFILTER.ID)))
                        {
                            masterFilter.SELECTEDFILTER = Apps.dbContext.MasterFilters.FirstOrDefault(c => c.ID.Equals(updateFilter.SELECTEDFILTER.ID));
                        }

                        if (updateFilter.SELECTEDENUM != null)
                        {
                            masterFilter.SELECTEDENUM = Apps.dbContext.MainEnum.Where(c => c.ID.Equals(updateFilter.SELECTEDENUM.ID)).FirstOrDefault();
                        }

                        if (updateFilter.MARKETPRICE != null)
                        {
                            masterFilter.MARKETPRICE = Apps.dbContext.MarketPrice.Where(c => c.ID.Equals(updateFilter.ID)).FirstOrDefault();
                        }

                        #region Check for newly added

                        if (updateFilter.LSTREGSTORE != null)
                        {
                            List<Stores> lstStoresAdd = updateFilter.LSTREGSTORE.Where(c => !masterFilter.LSTREGSTORE.Select(d => d.ID).Contains(c.ID)).ToList();
                            if (lstStoresAdd != null && lstStoresAdd.Count > 0)
                            {
                                lstStoresAdd.ForEach(c =>
                                {
                                    Stores fetchedStore = Apps.dbContext.Stores.Where(d => d.ID.Equals(c.ID)).FirstOrDefault();
                                    masterFilter.LSTREGSTORE.Add(fetchedStore);
                                });
                            }
                        }

                        if (updateFilter.LSTAPPLIEDSUBCATEGORIES != null)
                        {
                            List<MasterCategories> lstSubCategories = updateFilter.LSTAPPLIEDSUBCATEGORIES.Where(c => !masterFilter.LSTAPPLIEDSUBCATEGORIES.Select(d => d.ID).Contains(c.ID)).ToList();
                            if (lstSubCategories != null && lstSubCategories.Count > 0)
                            {
                                lstSubCategories.ForEach(c =>
                                {
                                    MasterCategories fetchedSubCategory = Apps.dbContext.MasterCategories.Where(d => d.ID.Equals(c.ID)).FirstOrDefault();
                                    masterFilter.LSTAPPLIEDSUBCATEGORIES.Add(fetchedSubCategory);
                                });
                            }
                        }

                        if (updateFilter.LSTSUBFILTERS != null)
                        {
                            List<MasterFilters> lstSubFilters = updateFilter.LSTSUBFILTERS.Where(c => !masterFilter.LSTSUBFILTERS.Select(d => d.ID).Contains(c.ID)).ToList();
                            if (lstSubFilters != null && lstSubFilters.Count > 0)
                            {
                                lstSubFilters.ForEach(c =>
                                {
                                    MasterFilters fetchedSubFilter = Apps.dbContext.MasterFilters.Where(d => d.ID.Equals(c.ID)).FirstOrDefault();
                                    masterFilter.LSTSUBFILTERS.Add(fetchedSubFilter);
                                });
                            }
                        }

                        #endregion

                        #region Check for newly deleted

                        if (masterFilter.LSTREGSTORE != null)
                        {
                            List<Stores> lstStoresRemove = masterFilter.LSTREGSTORE.Where(c => !updateFilter.LSTREGSTORE.Select(d => d.ID).Contains(c.ID)).ToList();
                            if (lstStoresRemove != null && lstStoresRemove.Count > 0)
                            {
                                lstStoresRemove.ForEach(c =>
                                {
                                    masterFilter.LSTREGSTORE.Remove(c);
                                });
                            }
                        }

                        if (masterFilter.LSTAPPLIEDSUBCATEGORIES != null)
                        {
                            List<MasterCategories> lstSubCateogiresRemove = masterFilter.LSTAPPLIEDSUBCATEGORIES.Where(c => !updateFilter.LSTAPPLIEDSUBCATEGORIES.Select(d => d.ID).Contains(c.ID)).ToList();
                            if (lstSubCateogiresRemove != null && lstSubCateogiresRemove.Count > 0)
                            {
                                lstSubCateogiresRemove.ForEach(c =>
                                {
                                    masterFilter.LSTAPPLIEDSUBCATEGORIES.Remove(c);
                                });
                            }
                        }

                        if (masterFilter.LSTSUBFILTERS != null)
                        {
                            List<MasterFilters> lstSubFiltersRemove = masterFilter.LSTSUBFILTERS.Where(c => !updateFilter.LSTSUBFILTERS.Select(d => d.ID).Contains(c.ID)).ToList();
                            if (lstSubFiltersRemove != null && lstSubFiltersRemove.Count > 0)
                            {
                                lstSubFiltersRemove.ForEach(c =>
                                {
                                    masterFilter.LSTSUBFILTERS.Remove(c);
                                });
                            }
                        }
                        #endregion

                        Apps.dbContext.SaveChanges();
                        Apps.Logger.Info("Master Filter ID: " + masterFilter.ID);
                        result = Messages.Succcess;
                    }
                    else
                    {
                        result = Messages.Filter + " " + Messages.DoesnotExists;
                    }
                }
                catch (Exception ex)
                {
                    Apps.Logger.ErrorException("Exception while UpdateMasterFilter: " + ex.Message, ex);
                    result = ex.Message;
                }
            }
            else
            {
                result = Messages.ObjectShouldNotBeNull;
            }
            Apps.Logger.Info("Result: " + result);
            return result;
        }

        internal static string DeleteMasterFilter(MasterFilters deleteFilter)
        {
            string result = string.Empty;
            try
            {
                if (deleteFilter != null)
                {
                    MasterFilters masterFilter = Apps.dbContext.MasterFilters.Where(c => c.ID.Equals(deleteFilter.ID)).FirstOrDefault();
                    if (masterFilter != null)
                    {
                        masterFilter.ISACTIVE = true;
                        masterFilter.VERSION += 1;
                        Apps.dbContext.SaveChanges();
                        Apps.Logger.Info("Master Filter ID: " + masterFilter.ID);
                        result = Messages.Succcess;
                    }
                    else
                    {
                        result = Messages.Filter + " " + Messages.DoesnotExists;
                    }
                }
                else
                {
                    result = Messages.ObjectShouldNotBeNull;
                }
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while DeleteMasterFilter: " + ex.Message, ex);
                result = ex.Message;
            }
            Apps.Logger.Info("Result: " + result);
            return result;
        }
    }
}
