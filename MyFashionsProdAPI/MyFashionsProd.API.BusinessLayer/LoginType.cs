﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer
{
    public enum LoginType
    {
        SuperAdmin = 0,
        Company = 1,
        Store = 2
    }
}
