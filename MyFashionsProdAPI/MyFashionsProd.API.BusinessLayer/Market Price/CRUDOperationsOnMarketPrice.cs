﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Market_Price
{
    public class CRUDOperationsOnMarketPrice
    {
        internal static string AddMarketPrice(MasterFilters masterFilter)
        {
            string result = string.Empty;
            try
            {
                if (masterFilter != null && masterFilter.MARKETPRICE != null)
                {
                    MasterFilters fetchedFilter = Apps.dbContext.MasterFilters.Where(c => c.ID.Equals(masterFilter.ID) && c.NAME.Equals(masterFilter.NAME)).FirstOrDefault();
                    if(fetchedFilter.MARKETPRICE!=null && fetchedFilter.MARKETPRICE.ID>0)
                    {
                        return Messages.MarketPrice + " " + Messages.AlreadyExists;
                    }
                    else
                    {
                        MarketPrice marketPrice = new MarketPrice();
                        marketPrice = PopulateMarketPriceFields.AssignMarketPriceValues(marketPrice, masterFilter.MARKETPRICE);
                        marketPrice.ID = Apps.dbContext.MarketPrice.ToList().Count > 0 ? (Apps.dbContext.MarketPrice.Max(c => c.ID) + 1) : 1;
                        marketPrice.VERSION = 1;
                        fetchedFilter.MARKETPRICE = marketPrice;
                        Apps.dbContext.SaveChanges();

                        result = Messages.Succcess;
                    }
                }
                else
                {
                    result = Messages.ObjectShouldNotBeNull;
                }
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while AddMarketPrice: " + ex.Message, ex);
                result = ex.Message;
            }
            Apps.Logger.Info("Result: " + result);
            return result;
        }

        internal static string UpdateMarketPrice(MasterFilters masterFilter)
        {
            string result = string.Empty;
            try
            {
                if (masterFilter != null && masterFilter.MARKETPRICE != null)
                {
                    MasterFilters fetchedFilter = Apps.dbContext.MasterFilters.Where(c => c.ID.Equals(masterFilter.ID) && c.NAME.Equals(masterFilter.NAME)).FirstOrDefault();
                    if (fetchedFilter.MARKETPRICE == null && fetchedFilter.MARKETPRICE.ID.Equals(0))
                    {
                        return Messages.MarketPrice + " " + Messages.DoesnotExists;
                    }
                    else
                    {
                        int currentVersion = fetchedFilter.VERSION;
                        fetchedFilter.MARKETPRICE = PopulateMarketPriceFields.AssignMarketPriceValues(fetchedFilter.MARKETPRICE, masterFilter.MARKETPRICE);
                        fetchedFilter.MARKETPRICE.VERSION = currentVersion + 1;
                        Apps.dbContext.SaveChanges();

                        result = Messages.Succcess;
                    }
                }
                else
                {
                    result = Messages.ObjectShouldNotBeNull;
                }
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while UpdateMarketPrice: " + ex.Message, ex);
                result = ex.Message;
            }
            Apps.Logger.Info("Result: " + result);
            return result;
        }

        internal static string DeleteMarketPrice(MasterFilters masterFilter)
        {
            string result = string.Empty;
            try
            {
                if (masterFilter != null && masterFilter.MARKETPRICE != null)
                {
                    MasterFilters fetchedFilter = Apps.dbContext.MasterFilters.Where(c => c.ID.Equals(masterFilter.ID) && c.NAME.Equals(masterFilter.NAME)).FirstOrDefault();
                    if (fetchedFilter.MARKETPRICE == null && fetchedFilter.MARKETPRICE.ID.Equals(0))
                    {
                        return Messages.MarketPrice + " " + Messages.DoesnotExists;
                    }
                    else
                    {
                        fetchedFilter.MARKETPRICE = null;
                        Apps.dbContext.SaveChanges();

                        result = Messages.Succcess;
                    }
                }
                else
                {
                    result = Messages.ObjectShouldNotBeNull;
                }
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while DeleteMarketPrice: " + ex.Message, ex);
                result = ex.Message;
            }
            Apps.Logger.Info("Result: " + result);
            return result;
        }
    }
}
