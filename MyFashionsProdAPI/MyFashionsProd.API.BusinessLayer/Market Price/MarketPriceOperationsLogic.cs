﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Market_Price
{
    public class MarketPriceOperationsLogic
    {
        #region Get Market Price
        public MarketPrice GetMarketPriceUsingFilterID(long filterID, out string errorMesage)
        {
            Apps.Logger.Info("Initiating GetMarketPriceUsingFilterID. Filter ID: " + filterID);
            errorMesage = string.Empty;
            MarketPrice marketPrice = new MarketPrice();
            MasterFilters fetchedFilter = Apps.dbContext.MasterFilters.Where(c => c.ID.Equals(filterID)).FirstOrDefault();
            if (fetchedFilter != null)
            {
                if (fetchedFilter.MARKETPRICE != null && fetchedFilter.ID > 0 && !string.IsNullOrEmpty(fetchedFilter.NAME))
                {
                    marketPrice = PopulateMarketPriceFields.AssignMarketPriceValues(marketPrice, fetchedFilter.MARKETPRICE);
                }
                else
                {
                    errorMesage = Messages.MarketPrice + " " + Messages.DoesnotExists + " " + Messages.Related + " " + Messages.To + " " + Messages.Filter;
                }
            }
            else
            {
                errorMesage = Messages.Filter + " " + Messages.DoesnotExists;
            }
            if (!string.IsNullOrEmpty(errorMesage))
                Apps.Logger.Info("Error Message: " + errorMesage);
            else
                Apps.Logger.Info("Result: " + Messages.Succcess);
            return marketPrice;
        }
        #endregion

        public string AddMarketPrice(MasterFilters masterFilter)
        {
            try
            {
                Apps.Logger.Info("Initiating AddMarketPrice.");
                string result = ValidateMarketPrice(masterFilter, "A");
                if(result.Equals(Messages.Succcess))
                {
                    result = CRUDOperationsOnMarketPrice.AddMarketPrice(masterFilter);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while AddMarketPrice: " + ex.Message, ex);
                return ex.Message;
            }
        }

        public string UpdateMarketPrice(MasterFilters masterFilter)
        {
            try
            {
                Apps.Logger.Info("Initiating UpdateMarketPrice."); 
                string result = ValidateMarketPrice(masterFilter, "U");
                Apps.Logger.Info("MasterFilterID: " + masterFilter.ID + " and MarketPrice ID: " + masterFilter.MARKETPRICE.ID);
                if (result.Equals(Messages.Succcess))
                {
                    result = CRUDOperationsOnMarketPrice.UpdateMarketPrice(masterFilter);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while UpdateMarketPrice: " + ex.Message, ex);
                return ex.Message;
            }
        }

        public string DeleteMarketPrice(MasterFilters masterFilter)
        {
            try
            {
                Apps.Logger.Info("Initiating DeleteMarketPrice.");
                string result = ValidateMarketPrice(masterFilter, "D");
                Apps.Logger.Info("MasterFilterID: " + masterFilter.ID + " and MarketPrice ID: " + masterFilter.MARKETPRICE.ID);
                if(result.Equals(Messages.Succcess))
                {
                    result = CRUDOperationsOnMarketPrice.DeleteMarketPrice(masterFilter);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while DeleteMarketPrice: " + ex.Message, ex);
                return ex.Message;
            }
        }

        public string ValidateMarketPrice(MasterFilters masterFilter, string code)
        {
            switch (code)
            {
                case "A":
                    MasterFilters fetchFilter = Apps.dbContext.MasterFilters.Where(c => c.ID.Equals(masterFilter.ID) && c.NAME.Equals(masterFilter.NAME)).FirstOrDefault();
                    if (fetchFilter != null)
                    {
                        if (fetchFilter.MARKETPRICE != null && fetchFilter.MARKETPRICE.ID > 0)
                        {
                            return Messages.MarketPrice + " " + Messages.AlreadyExists;
                        }
                        else
                        {
                            return Messages.Succcess;
                        }
                    }
                    else
                    {
                        return Messages.Filter + " " + Messages.DoesnotExists;
                    }
                case "U":
                case "D":
                    MasterFilters fetchedFilter = Apps.dbContext.MasterFilters.Where(c => c.ID.Equals(masterFilter.ID) && c.NAME.Equals(masterFilter.NAME)).FirstOrDefault();
                    if (fetchedFilter != null)
                    {
                        if (fetchedFilter.MARKETPRICE != null && fetchedFilter.MARKETPRICE.ID > 0)
                        {
                            return Messages.Succcess;
                        }
                        else
                        {
                            return Messages.MarketPrice + " " + Messages.DoesnotExists;
                        }
                    }
                    else
                    {
                        return Messages.Filter + " " + Messages.DoesnotExists;
                    }
                default:
                    return Messages.InvalidCRUDOperation;
            }
        }
    }
}
