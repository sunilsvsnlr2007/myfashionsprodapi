﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer
{
    public class Messages
    {
        public const string SuperAdmin = "Super Admin";
        public const string CateogryMsg = "Category";
        public const string SubCateogryMsg = "Sub Category";
        public const string ProductMsg = "Product";
        public const string TopCategory = "Top Category";
        public const string Offer = "Offer";
        public const string Filter = "Filter";
        public const string MainEnum = "MainEnum";
        public const string Company = "Company";
        public const string StoreMsg = "Store";
        public const string MarketPrice = "MarketPrice";
        public const string CompleteLook = "Complete Look";
        public const string SimilarProduct = "Similar Product";
        public const string Related = "Related";
        public const string Attached = "Attached";
        public const string PermissionDenied = "You are not authorized to process.";

        public const string Or = "Or";
        public const string And = "And";
        public const string Not = "Not";
        public const string To = "To";
        
        public const string InvalidUserName = "Invalid UserName.";
        public const string InvalidPassword = "Invalid Password.";
        public const string InvalidObject = "Invalid object or object has been modified by other source.";
        public const string UnKnown = "UnKnown";
        public const string UnderDevelopement = "Under Development.";
        public const string CompanyIDLoginDoesNotMatch = "Company ID does not match with Login Company";
        public const string StoreIDLoginDoesNotMatch = "Store ID does not match with Login Store";
        public const string Succcess = "Success.";
        public const string Failed = "Failed.";
        public const string FailedUpdatingPassword = "Failed updating password";
        public const string RequiredFieldsEmpty = "Required fields are empty.";
        public const string AlreadyExists = "Already exists. Please choose another name.";
        public const string DoesnotExists = "Does not exist.";
        public const string UnableToFind = "Unable to find";
        public const string NotAuthorize = "Unauthorized Access.";
        public const string RunInitiateSeed = "Please run initiate seed.";
        public const string InvalidCRUDOperation = "Invalid CRUD operation.";
        public const string RelationFailed = "Relation Mismatch or Not Exist.";
        public const string ObjectShouldNotBeNull = "Object should not be null.";
        
    }
}
