﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Offers
{
    public class CRUDOperationsOnMasterOffers
    {
        internal static string AddMasterOffer(MasterOffers addOffer)
        {
            string result = string.Empty;
            if (addOffer != null)
            {
                try
                {
                    MasterOffers mainMasterOffer = new MasterOffers();
                    mainMasterOffer = PopulateOfferFields.AssignFieldsForCreateOffer(mainMasterOffer, addOffer);
                    mainMasterOffer.ID = Apps.dbContext.MasterOffers.ToList().Count > 0 ? (Apps.dbContext.MasterOffers.Max(c => c.ID) + 1) : 1;
                    mainMasterOffer.VERSION = 1;
                    mainMasterOffer.REGCOMPANY = Apps.dbContext.Company.Where(c => c.ID.Equals(addOffer.REGCOMPANY.ID)).FirstOrDefault();

                    if (addOffer.SELECTEDOFFER != null && addOffer.SELECTEDOFFER.ID > 0 && Apps.dbContext.MasterOffers.Any(c => c.ID.Equals(addOffer.SELECTEDOFFER.ID)))
                    {
                        mainMasterOffer.SELECTEDOFFER = Apps.dbContext.MasterOffers.FirstOrDefault(c => c.ID.Equals(addOffer.SELECTEDOFFER.ID));
                    }

                    if (addOffer.SELECTEDENUM != null)
                    {
                        mainMasterOffer.SELECTEDENUM = Apps.dbContext.MainEnum.Where(c => c.ID.Equals(addOffer.SELECTEDENUM.ID)).FirstOrDefault();
                    }

                    if (addOffer.REGCOMPANY != null && addOffer.REGCOMPANY.ID > 0)
                    {
                        if (addOffer.LSTAPPLIEDSUBCATEGORIES != null && addOffer.LSTAPPLIEDSUBCATEGORIES.Count > 0)
                        {
                            addOffer.LSTAPPLIEDSUBCATEGORIES.Where(c => c.ISACTIVE.Equals(true)).ToList().ForEach(c =>
                                {
                                    MasterCategories fetchedCateg = Apps.dbContext.MasterCategories.Where(d => d.ID.Equals(c.ID)).FirstOrDefault();
                                    mainMasterOffer.LSTAPPLIEDSUBCATEGORIES.Add(fetchedCateg);
                                });
                        }

                        if (addOffer.LSTREGSTORE != null && addOffer.LSTREGSTORE.Count > 0)
                        {
                            addOffer.LSTREGSTORE.Where(c => c.ISACTIVE.Equals(true)).ToList().ForEach(c =>
                                {
                                    Stores fetchedStore = Apps.dbContext.Stores.Where(d => d.ID.Equals(c.ID)).FirstOrDefault();
                                    mainMasterOffer.LSTREGSTORE.Add(fetchedStore);
                                });
                        }

                        if (addOffer.LSTSUBOFFERS != null && addOffer.LSTSUBOFFERS.Count > 0)
                        {
                            addOffer.LSTSUBOFFERS.Where(c => c.ISEXPIRED.Equals(false)).ToList().ForEach(c =>
                                {
                                    MasterOffers fetchedOffer = Apps.dbContext.MasterOffers.Where(d => d.ID.Equals(c.ID)).FirstOrDefault();
                                    mainMasterOffer.LSTSUBOFFERS.Add(fetchedOffer);
                                });
                        }

                        Apps.dbContext.MasterOffers.Add(mainMasterOffer);
                        Apps.dbContext.SaveChanges();
                        Apps.Logger.Info("Master Offer ID: " + mainMasterOffer.ID);
                        result = Messages.Succcess;
                    }
                    else
                    {
                        result = Messages.Company + " " + Messages.DoesnotExists;
                    }
                }
                catch (Exception ex)
                {
                    Apps.Logger.ErrorException("Exception while AddMasterOffer: " + ex.Message, ex);
                    result = ex.Message;
                }
            }
            else
            {
                result = Messages.ObjectShouldNotBeNull;
            }
            Apps.Logger.Info("Result: " + result);
            return result;
        }

        internal static string UpdateMasterOffer(MasterOffers updateOffer)
        {
            string result = string.Empty;
            if (updateOffer != null)
            {
                try
                {
                    MasterOffers mainMasterOffer = Apps.dbContext.MasterOffers.FirstOrDefault(c => c.ID.Equals(updateOffer.ID));
                    if (mainMasterOffer != null)
                    {
                        int currentVersion = mainMasterOffer.VERSION;
                        mainMasterOffer = PopulateOfferFields.AssignFieldsForCreateOffer(mainMasterOffer, updateOffer);
                        mainMasterOffer.VERSION = currentVersion + 1;
                        mainMasterOffer.REGCOMPANY = Apps.dbContext.Company.FirstOrDefault(c => c.ID.Equals(updateOffer.REGCOMPANY.ID));

                        if (updateOffer.SELECTEDOFFER != null && updateOffer.SELECTEDOFFER.ID > 0 && Apps.dbContext.MasterOffers.Any(c => c.ID.Equals(updateOffer.SELECTEDOFFER.ID)))
                        {
                            mainMasterOffer.SELECTEDOFFER = Apps.dbContext.MasterOffers.FirstOrDefault(c => c.ID.Equals(updateOffer.SELECTEDOFFER.ID));
                        }

                        if (updateOffer.SELECTEDENUM != null)
                        {
                            mainMasterOffer.SELECTEDENUM = Apps.dbContext.MainEnum.FirstOrDefault(c => c.ID.Equals(updateOffer.SELECTEDENUM.ID));
                        }

                        #region Check for newly added
                        if (updateOffer.LSTREGSTORE != null)
                        {
                            List<Stores> lstStoresAdd = updateOffer.LSTREGSTORE.Where(c => !mainMasterOffer.LSTREGSTORE.Select(d => d.ID).Contains(c.ID)).ToList();
                            if (lstStoresAdd != null && lstStoresAdd.Count > 0)
                            {
                                lstStoresAdd.ForEach(c =>
                                    {
                                        Stores fetchedStore = Apps.dbContext.Stores.FirstOrDefault(d => d.ID.Equals(c.ID));
                                        if (fetchedStore != null)
                                        {
                                            mainMasterOffer.LSTREGSTORE.Add(fetchedStore);
                                        }
                                    });
                            }
                        }

                        if (updateOffer.LSTAPPLIEDSUBCATEGORIES != null)
                        {
                            List<MasterCategories> lstSubCategories = updateOffer.LSTAPPLIEDSUBCATEGORIES.Where(c => !mainMasterOffer.LSTAPPLIEDSUBCATEGORIES.Select(d => d.ID).Contains(c.ID)).ToList();
                            if (lstSubCategories != null && lstSubCategories.Count > 0)
                            {
                                lstSubCategories.ForEach(c =>
                                {
                                    MasterCategories fetchedSubCategory = Apps.dbContext.MasterCategories.FirstOrDefault(d => d.ID.Equals(c.ID));
                                    if (fetchedSubCategory != null)
                                    {
                                        mainMasterOffer.LSTAPPLIEDSUBCATEGORIES.Add(fetchedSubCategory);
                                    }
                                });
                            }
                        }

                        if (updateOffer.LSTSUBOFFERS != null)
                        {
                            List<MasterOffers> lstSubOffers = updateOffer.LSTSUBOFFERS.Where(c => !mainMasterOffer.LSTSUBOFFERS.Select(d => d.ID).Contains(c.ID)).ToList();
                            if (lstSubOffers != null && lstSubOffers.Count > 0)
                            {
                                lstSubOffers.ForEach(c =>
                                {
                                    MasterOffers fetchedSubOffer = Apps.dbContext.MasterOffers.FirstOrDefault(d => d.ID.Equals(c.ID));
                                    if (fetchedSubOffer != null)
                                    {
                                        mainMasterOffer.LSTSUBOFFERS.Add(fetchedSubOffer);
                                    }
                                });
                            }
                        }

                        if (updateOffer.LSTPRODUCTS != null)
                        {
                            List<Product> lstProducts = updateOffer.LSTPRODUCTS.Where(c => !mainMasterOffer.LSTPRODUCTS.Select(d => d.ID).Contains(c.ID)).ToList();
                            if (lstProducts != null && lstProducts.Count > 0)
                            {
                                lstProducts.ForEach(c =>
                                    {
                                        Product fetchedProd = Apps.dbContext.Product.FirstOrDefault(d => d.ID.Equals(c.ID));
                                        if(fetchedProd!=null)
                                        {
                                            mainMasterOffer.LSTPRODUCTS.Add(fetchedProd);
                                        }
                                    });
                            }
                        }

                        #endregion

                        #region Check for newly deleted
                        if (mainMasterOffer.LSTREGSTORE != null)
                        {
                            List<Stores> lstStoresRemove = mainMasterOffer.LSTREGSTORE.Where(c => !updateOffer.LSTREGSTORE.Select(d => d.ID).Contains(c.ID)).ToList();
                            if (lstStoresRemove != null && lstStoresRemove.Count > 0)
                            {
                                lstStoresRemove.ForEach(c =>
                                    {
                                        mainMasterOffer.LSTREGSTORE.Remove(c);
                                    });
                            }
                        }

                        if (mainMasterOffer.LSTAPPLIEDSUBCATEGORIES != null)
                        {
                            List<MasterCategories> lstSubCateogiresRemove = mainMasterOffer.LSTAPPLIEDSUBCATEGORIES.Where(c => !updateOffer.LSTAPPLIEDSUBCATEGORIES.Select(d => d.ID).Contains(c.ID)).ToList();
                            if (lstSubCateogiresRemove != null && lstSubCateogiresRemove.Count > 0)
                            {
                                lstSubCateogiresRemove.ForEach(c =>
                                {
                                    mainMasterOffer.LSTAPPLIEDSUBCATEGORIES.Remove(c);
                                });
                            }
                        }

                        if (mainMasterOffer.LSTSUBOFFERS != null)
                        {
                            List<MasterOffers> lstSubOffersRemove = mainMasterOffer.LSTSUBOFFERS.Where(c => !updateOffer.LSTSUBOFFERS.Select(d => d.ID).Contains(c.ID)).ToList();
                            if (lstSubOffersRemove != null && lstSubOffersRemove.Count > 0)
                            {
                                lstSubOffersRemove.ForEach(c =>
                                {
                                    mainMasterOffer.LSTSUBOFFERS.Remove(c);
                                });
                            }
                        }

                        if (mainMasterOffer.LSTPRODUCTS != null)
                        {
                            List<Product> lstProductsRemove = mainMasterOffer.LSTPRODUCTS.Where(c => !updateOffer.LSTPRODUCTS.Select(d => d.ID).Contains(c.ID)).ToList();
                            if (lstProductsRemove != null && lstProductsRemove.Count > 0)
                            {
                                lstProductsRemove.ForEach(c =>
                                    {
                                        mainMasterOffer.LSTPRODUCTS.Remove(c);
                                    });
                            }
                        }

                        #endregion

                        Apps.dbContext.SaveChanges();
                        Apps.Logger.Info("Master Offer ID: " + mainMasterOffer.ID);
                        result = Messages.Succcess;
                    }
                    else
                    {
                        result = Messages.Offer + " " + Messages.DoesnotExists;
                    }
                }
                catch (Exception ex)
                {
                    Apps.Logger.ErrorException("Exception while UpdateMasterOffer: " + ex.Message, ex);
                    result = ex.Message;
                }
            }
            else
            {
                result = Messages.ObjectShouldNotBeNull;
            }
            Apps.Logger.Info("Result: " + result);
            return result;
        }

        internal static string DeleteMasterOffer(MasterOffers deletedOffer)
        {
            string result = string.Empty;
            try
            {   
                if (deletedOffer != null)
                {
                    MasterOffers mainMasterOffer = Apps.dbContext.MasterOffers.Where(c => c.ID.Equals(deletedOffer.ID)).FirstOrDefault();
                    if (mainMasterOffer != null)
                    {
                        mainMasterOffer.ISEXPIRED = true;
                        mainMasterOffer.VERSION += 1;
                        mainMasterOffer.LASTUPDATEDTIME = DateTime.Now;
                        Apps.dbContext.SaveChanges();
                        Apps.Logger.Info("Master Offer ID: " + mainMasterOffer.ID);
                        result = Messages.Succcess;
                    }
                    else
                    {
                        result = Messages.Offer + " " + Messages.DoesnotExists;
                    }
                }
                else
                {
                    result = Messages.ObjectShouldNotBeNull;
                }
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while DeleteMasterOffer: " + ex.Message, ex);
                result = ex.Message;
            }
            Apps.Logger.Info("Result: " + result);
            return result;
        }
    }
}