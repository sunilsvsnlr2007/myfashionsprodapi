﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Product_Logic
{
    public class CRUDOperationsOnProductAdd
    {
        internal static string AddProduct(Product prod)
        {
            string result = string.Empty;
            try
            {
                if (prod != null)
                {
                    if (!Apps.dbContext.Product.Any(c => c.ID.Equals(prod.ID) && c.IMAGENAME.Equals(prod.IMAGENAME)))
                    {
                        Product masterProd = new Product();
                        masterProd = PopulateProductFields.AssignProductValues(masterProd, prod, true);
                        masterProd.ID = Apps.dbContext.Product.ToList().Count > 0 ? (Apps.dbContext.Product.Max(c => c.ID) + 1) : 1;
                        masterProd.VERSION = 1;
                        masterProd.LASTUPDATEDTIME = DateTime.Now;
                        masterProd.RELATEDSTORE = Apps.dbContext.Stores.FirstOrDefault(c => c.ID.Equals(prod.RELATEDSTORE.ID));

                        result = FilterRelatedCalculationObject(prod, result, masterProd);

                        ProductImageCollectionObject(prod, masterProd);

                        RelatedCategoriesObject(prod, masterProd);

                        RelatedFiltersObject(prod, masterProd);

                        if (string.IsNullOrEmpty(result))
                        {
                            Apps.dbContext.Product.Add(masterProd);
                            Apps.dbContext.SaveChanges();
                            result = Messages.Succcess;
                        }
                    }
                    else
                    {
                        result = Messages.ProductMsg + " " + Messages.AlreadyExists;
                    }
                }
                else
                {
                    result = Messages.ObjectShouldNotBeNull;
                }
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while AddProduct: " + ex.Message, ex);
                result = ex.Message;
            }
            Apps.Logger.Info("Result: " + result);
            return result;
        }

        private static void RelatedFiltersObject(Product prod, Product masterProd)
        {
            if (prod.RELATEDFILTERS != null && prod.RELATEDFILTERS.Count > 0)
            {
                if (masterProd.RELATEDFILTERS == null)
                    masterProd.RELATEDFILTERS = new List<MasterFilters>();
                prod.RELATEDFILTERS.ToList().ForEach(c =>
                {
                    MasterFilters masterFilter = Apps.dbContext.MasterFilters.FirstOrDefault(d => d.ID.Equals(c.ID));
                    if (masterFilter != null)
                        masterProd.RELATEDFILTERS.Add(masterFilter);
                });
            }
        }

        private static void RelatedCategoriesObject(Product prod, Product masterProd)
        {
            if (prod.RELATEDCATEGORIES != null && prod.RELATEDCATEGORIES.Count > 0)
            {
                if (masterProd.RELATEDCATEGORIES == null)
                    masterProd.RELATEDCATEGORIES = new List<MasterCategories>();
                prod.RELATEDCATEGORIES.ToList().ForEach(c =>
                {
                    MasterCategories masterCateg = Apps.dbContext.MasterCategories.FirstOrDefault(d => d.ID.Equals(c.ID));
                    if (masterCateg != null)
                        masterProd.RELATEDCATEGORIES.Add(masterCateg);
                });
            }
        }

        private static void ProductImageCollectionObject(Product prod, Product masterProd)
        {
            if (prod.LSTPRODUCTIMAGES != null && prod.LSTPRODUCTIMAGES.Count > 0)
            {
                if (masterProd.LSTPRODUCTIMAGES == null)
                    masterProd.LSTPRODUCTIMAGES = new List<ProductImageCollection>();
                foreach (ProductImageCollection item in prod.LSTPRODUCTIMAGES)
                {
                    ProductImageCollection prodImgCollect = new ProductImageCollection();
                    prodImgCollect = PopulateProductImageCollectionFields.AssignProductImageCollectionValues(prodImgCollect, item);
                    prodImgCollect.ID = Apps.dbContext.ProductImageCollection.ToList().Count > 0 ? (Apps.dbContext.ProductImageCollection.Max(c => c.ID) + 1) : 1;
                    prodImgCollect.VERSION = 1;
                    prodImgCollect.LASTUPDATEDTIME = DateTime.Now;
                    masterProd.LSTPRODUCTIMAGES.Add(prodImgCollect);
                }
            }
        }

        private static string FilterRelatedCalculationObject(Product prod, string result, Product masterProd)
        {
            if (prod.FILTERSRELATEDTOCALC != null && prod.FILTERSRELATEDTOCALC.Count > 0)
            {
                if (masterProd.FILTERSRELATEDTOCALC == null)
                    masterProd.FILTERSRELATEDTOCALC = new List<FilterCalculation>();
                foreach (FilterCalculation item in prod.FILTERSRELATEDTOCALC)
                {
                    FilterCalculation filterCal = new FilterCalculation();
                    filterCal = PopulateFilterCalculationFields.AssignFilterCalculationFields(filterCal, item);
                    filterCal.ID = Apps.dbContext.FilterCalculation.ToList().Count > 0 ? (Apps.dbContext.FilterCalculation.Max(c => c.ID) + 1) : 1;
                    filterCal.VERSION = 1;
                    if (Apps.dbContext.MasterFilters.Any(c => c.ID.Equals(item.MASTERFILTER.ID) && c.NAME.Equals(item.MASTERFILTER.NAME)))
                    {
                        filterCal.MASTERFILTER = Apps.dbContext.MasterFilters.FirstOrDefault(c => c.ID.Equals(item.MASTERFILTER.ID) && c.NAME.Equals(item.MASTERFILTER.NAME));
                        masterProd.FILTERSRELATEDTOCALC.Add(filterCal);
                    }
                    else
                    {
                        result = Messages.Filter + " " + Messages.DoesnotExists;
                        break;
                    }
                }
            }
            return result;
        }
    }
}
