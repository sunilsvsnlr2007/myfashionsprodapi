﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Product_Logic
{
    public class CRUDOperationsOnProductUpdate
    {
        internal static string UpdateProduct(Product prod)
        {
            string result = string.Empty;
            try
            {
                if (prod != null)
                {
                    if (Apps.dbContext.Product.Any(c => c.ID.Equals(prod.ID)))
                    {
                        Product masterProd = Apps.dbContext.Product.FirstOrDefault(c => c.ID.Equals(prod.ID));
                        int currentVersion = masterProd.VERSION;
                        masterProd = PopulateProductFields.AssignProductValues(masterProd, prod, true);
                        masterProd.VERSION = currentVersion + 1;
                        masterProd.LASTUPDATEDTIME = DateTime.Now;

                        result = FilterRelatedCalculationObject(prod, result, masterProd);

                        if (string.IsNullOrEmpty(result))
                        {
                            if (Apps.dbContext.Stores.Any(c => c.ID.Equals(masterProd.RELATEDSTORE.ID)))
                            {
                                masterProd.RELATEDSTORE = Apps.dbContext.Stores.FirstOrDefault(c => c.ID.Equals(prod.RELATEDSTORE.ID));
                            }
                            else
                            {
                                result = Messages.StoreMsg + " " + Messages.DoesnotExists;
                            }

                            ProductImageCollectionObject(prod, masterProd);

                            RelatedCategoriesObject(prod, masterProd);

                            RelatedFiltersObject(prod, masterProd);

                            Apps.dbContext.SaveChanges();
                            result = Messages.Succcess;

                            //This will update Complete Look and Similar Product initial image which is dependent on the current product.
                            result = Complete_Look_Logic.CRUDOperationsOnCompleteLook.UpdateCompleteLook(masterProd);
                            result = Similar_Products_Logic.CRUDOperationsOnSimilarProduct.UpdateSimilarProduct(masterProd);
                        }
                    }
                    else
                    {
                        result = Messages.ProductMsg + " " + Messages.DoesnotExists;
                    }
                }
                else
                {
                    result = Messages.ObjectShouldNotBeNull;
                }
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while UpdateProduct: " + ex.Message, ex);
                result = ex.Message;
            }
            Apps.Logger.Info("Result: " + result);
            return result;
        }

        private static void RelatedFiltersObject(Product prod, Product masterProd)
        {
            if ((prod.RELATEDFILTERS != null && prod.RELATEDFILTERS.Count > 0) || (masterProd.RELATEDFILTERS != null && masterProd.RELATEDFILTERS.Count > 0))
            {
                if (masterProd.RELATEDFILTERS == null)
                    masterProd.RELATEDFILTERS = new List<MasterFilters>();

                #region Check for newly added Master Filters

                List<MasterFilters> lstMasterFiltersAdd = prod.RELATEDFILTERS.Where(c => !masterProd.RELATEDFILTERS.Select(d => d.ID).Contains(c.ID)).ToList();
                if (lstMasterFiltersAdd != null && lstMasterFiltersAdd.Count > 0)
                {
                    lstMasterFiltersAdd.ForEach(c =>
                    {
                        MasterFilters masterFilter = Apps.dbContext.MasterFilters.FirstOrDefault(d => d.ID.Equals(c.ID) && d.NAME.Equals(c.NAME));
                        if (masterFilter != null)
                            masterProd.RELATEDFILTERS.Add(masterFilter);
                    });
                }

                #endregion

                #region Check for delete Master Filters

                List<MasterFilters> lstMasterFiltersRemove = masterProd.RELATEDFILTERS.Where(c => !prod.RELATEDFILTERS.Select(d => d.ID).Contains(c.ID)).ToList();
                if (lstMasterFiltersRemove != null && lstMasterFiltersRemove.Count > 0)
                {
                    lstMasterFiltersRemove.ForEach(c =>
                    {
                        MasterFilters masterFilter = Apps.dbContext.MasterFilters.FirstOrDefault(d => d.ID.Equals(c.ID) && d.NAME.Equals(c.NAME));
                        if (masterFilter != null)
                            masterProd.RELATEDFILTERS.Remove(masterFilter);
                    });
                }

                #endregion
            }
        }

        private static void RelatedCategoriesObject(Product prod, Product masterProd)
        {
            if ((prod.RELATEDCATEGORIES != null && prod.RELATEDCATEGORIES.Count > 0) || (masterProd.RELATEDCATEGORIES != null && masterProd.RELATEDCATEGORIES.Count > 0))
            {
                if (masterProd.RELATEDCATEGORIES == null)
                    masterProd.RELATEDCATEGORIES = new List<MasterCategories>();

                #region Check for newly added Categories

                List<MasterCategories> lstMasterCategoriesAdd = prod.RELATEDCATEGORIES.Where(c => !masterProd.RELATEDCATEGORIES.Select(d => d.ID).Contains(c.ID)).ToList();
                if (lstMasterCategoriesAdd != null && lstMasterCategoriesAdd.Count > 0)
                {
                    lstMasterCategoriesAdd.ForEach(c =>
                    {
                        MasterCategories masterCateg = Apps.dbContext.MasterCategories.FirstOrDefault(d => d.ID.Equals(c.ID) && d.NAME.Equals(c.NAME));
                        if (masterCateg != null)
                            masterProd.RELATEDCATEGORIES.Add(masterCateg);
                    });
                }

                #endregion

                #region Check for delete Categories

                List<MasterCategories> lstMasterCategoriesRemove = masterProd.RELATEDCATEGORIES.Where(c => !prod.RELATEDCATEGORIES.Select(d => d.ID).Contains(c.ID)).ToList();
                if (lstMasterCategoriesRemove != null && lstMasterCategoriesRemove.Count > 0)
                {
                    lstMasterCategoriesRemove.ForEach(c =>
                    {
                        MasterCategories masterCateg = Apps.dbContext.MasterCategories.FirstOrDefault(d => d.ID.Equals(c.ID) && d.NAME.Equals(c.NAME));
                        if (masterCateg != null)
                            masterProd.RELATEDCATEGORIES.Remove(masterCateg);
                    });
                }

                #endregion
            }
        }

        private static void ProductImageCollectionObject(Product prod, Product masterProd)
        {
            if ((prod.LSTPRODUCTIMAGES != null && prod.LSTPRODUCTIMAGES.Count > 0) || (masterProd.LSTPRODUCTIMAGES != null && masterProd.LSTPRODUCTIMAGES.Count > 0))
            {
                if (masterProd.LSTPRODUCTIMAGES == null)
                    masterProd.LSTPRODUCTIMAGES = new List<ProductImageCollection>();

                #region Checking for newly added Image Collection

                List<ProductImageCollection> lstProdImgCollectAdd = prod.LSTPRODUCTIMAGES.Where(c => !masterProd.LSTPRODUCTIMAGES.Select(d => d.ID).Contains(c.ID)).ToList();
                if (lstProdImgCollectAdd != null && lstProdImgCollectAdd.Count > 0)
                {
                    lstProdImgCollectAdd.ForEach(c =>
                    {
                        ProductImageCollection prodImgCollect = new ProductImageCollection();
                        prodImgCollect = PopulateProductImageCollectionFields.AssignProductImageCollectionValues(prodImgCollect, c);
                        prodImgCollect.ID = Apps.dbContext.ProductImageCollection.ToList().Count > 0 ? (Apps.dbContext.ProductImageCollection.Max(d => d.ID) + 1) : 1;
                        prodImgCollect.VERSION = 1;
                        prodImgCollect.LASTUPDATEDTIME = DateTime.Now;
                        masterProd.LSTPRODUCTIMAGES.Add(prodImgCollect);
                    });
                }

                #endregion

                #region Checking for delete image collection

                List<ProductImageCollection> lstProdImgCollectDelete = masterProd.LSTPRODUCTIMAGES.Where(c => !prod.LSTPRODUCTIMAGES.Select(d => d.ID).Contains(c.ID)).ToList();
                if (lstProdImgCollectDelete != null && lstProdImgCollectDelete.Count > 0)
                {
                    lstProdImgCollectDelete.ForEach(c =>
                    {
                        if (Apps.dbContext.ProductImageCollection.Any(d => d.ID.Equals(c.ID)))
                        {
                            masterProd.LSTPRODUCTIMAGES.Remove(c);
                        }
                    });
                }

                #endregion
            }
        }

        private static string FilterRelatedCalculationObject(Product prod, string result, Product masterProd)
        {
            if ((prod.FILTERSRELATEDTOCALC != null && prod.FILTERSRELATEDTOCALC.Count > 0) || (masterProd.FILTERSRELATEDTOCALC != null && masterProd.FILTERSRELATEDTOCALC.Count > 0))
            {
                if (masterProd.FILTERSRELATEDTOCALC == null)
                    masterProd.FILTERSRELATEDTOCALC = new List<FilterCalculation>();

                #region Check for Add Filter Related To Calculation Objects

                List<FilterCalculation> lstFilterCalAdd = prod.FILTERSRELATEDTOCALC.Where(c => !masterProd.FILTERSRELATEDTOCALC.Select(d => d.ID).Contains(c.ID)).ToList();

                if (lstFilterCalAdd != null && lstFilterCalAdd.Count > 0)
                {
                    foreach (FilterCalculation item in lstFilterCalAdd)
                    {
                        FilterCalculation filterCal = new FilterCalculation();
                        filterCal = PopulateFilterCalculationFields.AssignFilterCalculationFields(filterCal, item);
                        filterCal.ID = Apps.dbContext.FilterCalculation.ToList().Count > 0 ? (Apps.dbContext.FilterCalculation.Max(c => c.ID) + 1) : 1;
                        filterCal.VERSION = 1;
                        if (Apps.dbContext.MasterFilters.Any(c => c.ID.Equals(item.MASTERFILTER.ID) && c.NAME.Equals(item.MASTERFILTER.NAME)))
                        {
                            filterCal.MASTERFILTER = Apps.dbContext.MasterFilters.FirstOrDefault(c => c.ID.Equals(item.MASTERFILTER.ID) && c.NAME.Equals(item.MASTERFILTER.NAME));
                            masterProd.FILTERSRELATEDTOCALC.Add(filterCal);
                        }
                        else
                        {
                            result = Messages.Filter + " " + Messages.DoesnotExists;
                            break;
                        }
                    }
                }

                #endregion

                #region Check for Delete Filter Related To Calculation Objects
                
                if (string.IsNullOrEmpty(result))
                {
                    List<FilterCalculation> lstFilterCalRemove = masterProd.FILTERSRELATEDTOCALC.Where(c => !prod.FILTERSRELATEDTOCALC.Select(d => d.ID).Contains(c.ID)).ToList();
                    if (lstFilterCalAdd != null && lstFilterCalAdd.Count > 0)
                    {
                        lstFilterCalRemove.ForEach(c =>
                        {
                            masterProd.FILTERSRELATEDTOCALC.Remove(c);
                        });
                    }
                }

                #endregion
            }
            return result;
        }
    }
}
