﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Product_Logic
{
    public class ProductsOperationLogic
    {
        public List<Product> GetAllProductsUnderStore(long storeID, out string result)
        {
            Apps.Logger.Info("Initiating GetAllProductsUnderStore. Store ID: " + storeID);
            result = string.Empty;
            List<Product> lstProducts = new List<Product>();

            try
            {
                if (storeID > 0 && Apps.dbContext.Stores.Any(c => c.ID.Equals(storeID)))
                {
                    Apps.dbContext.Product.Where(c => c.RELATEDSTORE.ID.Equals(storeID)).ToList().ForEach(c =>
                        {
                            Product prod = new Product();
                            prod = PopulateProductFields.AssignProductValues(prod, c);
                            lstProducts.Add(prod);
                        });
                    Apps.Logger.Info("Qualified Products Count: " + lstProducts.Count);
                    result = Messages.Succcess;
                }
                else
                {
                    result = Messages.StoreMsg + " " + Messages.DoesnotExists;
                }
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while GetAllProductsUnderStore: " + ex.Message, ex);
                result = ex.Message;
            }

            Apps.Logger.Info("Result: " + result);
            return lstProducts;
        }

        public string AddProduct(Product prod)
        {
            try
            {
                Apps.Logger.Info("Initiating AddProduct.");
                string result = ValidateMasterProduct(prod, "A");
                if (result.Equals(Messages.Succcess))
                {
                    result = CRUDOperationsOnProductAdd.AddProduct(prod);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while AddProduct: " + ex.Message, ex);
                return ex.Message;
            }
        }

        public string UpdateProduct(Product prod)
        {
            try
            {
                Apps.Logger.Info("Initiating UpdateProduct.");
                string result = ValidateMasterProduct(prod, "U");
                if (result.Equals(Messages.Succcess))
                {
                    result = CRUDOperationsOnProductUpdate.UpdateProduct(prod);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while UpdateProduct: " + ex.Message, ex);
                return ex.Message;
            }
        }

        public string DeleteProduct(Product prod)
        {
            try
            {
                Apps.Logger.Info("Initiating DeleteProduct.");
                string result = ValidateMasterProduct(prod, "D");
                if (result.Equals(Messages.Succcess))
                {
                    result = CRUDOperationsOnProductDelete.DeleteProduct(prod);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while DeleteProduct: " + ex.Message, ex);
                return ex.Message;
            }
        }

        private string ValidateMasterProduct(Product prod, string parameter)
        {
            string result = string.Empty;
            switch (parameter)
            {
                case "A":
                    if (prod != null && !string.IsNullOrEmpty(prod.IMAGENAME) && prod.LSTPRODUCTIMAGES != null && prod.LSTPRODUCTIMAGES.Count > 0
                        && !string.IsNullOrEmpty(prod.NAME) && !string.IsNullOrEmpty(prod.PRODUCTTYPE) && prod.RELATEDCATEGORIES != null && prod.RELATEDCATEGORIES.Count > 0
                        && prod.RELATEDFILTERS != null && prod.RELATEDFILTERS.Count > 0 && prod.RELATEDSTORE != null && prod.RELATEDSTORE.ID > 0)
                    {
                        if (!Apps.dbContext.Product.Any(c => c.ID.Equals(prod.ID) && c.IMAGENAME.Equals(prod.IMAGENAME)))
                        {
                            result = ValidateProduct(prod, result);
                        }
                        else
                        {
                            result = Messages.ProductMsg + " " + Messages.AlreadyExists;
                        }
                    }
                    else
                    {
                        result = Messages.RequiredFieldsEmpty;
                    }
                    break;
                case "U":
                case "D":
                    if (prod != null && !string.IsNullOrEmpty(prod.IMAGENAME) && prod.ID > 0 && prod.SNO > 0 && prod.LSTPRODUCTIMAGES != null && prod.LSTPRODUCTIMAGES.Count > 0
                        && !string.IsNullOrEmpty(prod.NAME) && !string.IsNullOrEmpty(prod.PRODUCTTYPE) && prod.RELATEDCATEGORIES != null && prod.RELATEDCATEGORIES.Count > 0
                        && prod.RELATEDFILTERS != null && prod.RELATEDFILTERS.Count > 0 && prod.RELATEDSTORE != null && prod.RELATEDSTORE.ID > 0)
                    {
                        if (Apps.dbContext.Product.Any(c => c.ID.Equals(prod.ID) && c.NAME.Equals(prod.NAME)))
                        {
                            result = ValidateProduct(prod, result);
                        }
                        else
                        {
                            result = Messages.ProductMsg + " " + Messages.DoesnotExists;
                        }
                    }
                    else
                    {
                        result = Messages.RequiredFieldsEmpty;
                    }
                    break;
                default:
                    result = Messages.InvalidCRUDOperation;
                    break;
            }
            return result;
        }

        private static string ValidateProduct(Product prod, string result)
        {
            if (Apps.dbContext.Stores.Any(c => c.ID.Equals(prod.RELATEDSTORE.ID)))
            {
                foreach (MasterCategories masterCateg in prod.RELATEDCATEGORIES)
                {
                    if (!Apps.dbContext.MasterCategories.Any(c => c.ID.Equals(masterCateg.ID)))
                    {
                        result = Messages.CateogryMsg + " " + Messages.DoesnotExists;
                        break;
                    }
                }

                foreach (MasterFilters masterFilter in prod.RELATEDFILTERS)
                {
                    if (!Apps.dbContext.MasterCategories.Any(c => c.ID.Equals(masterFilter.ID)))
                    {
                        result = Messages.Filter + " " + Messages.DoesnotExists;
                        break;
                    }
                }

                if (string.IsNullOrEmpty(result))
                    result = Messages.Succcess;
            }
            else
            {
                result = Messages.StoreMsg + " " + Messages.DoesnotExists;
            }

            return result;
        }
    }
}
