﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Similar_Products_Logic
{
    public class CRUDOperationsOnSimilarProduct
    {
        internal static string AddOrUpdateSimilarProduct(Product prod)
        {
            string result = string.Empty;
            try
            {
                Product mainProd = Apps.dbContext.Product.Where(c => c.ID.Equals(prod.ID) && c.NAME.Equals(prod.NAME)).FirstOrDefault();
                if (mainProd != null)
                {
                    if (mainProd.LSTSIMILARPRODUCTS == null)
                        mainProd.LSTSIMILARPRODUCTS = new List<SimilarProduct>();

                    #region Add or Update Complete Look
                    prod.LSTSIMILARPRODUCTS.ToList().ForEach(c =>
                    {
                        if (!Apps.dbContext.SimilarProduct.Any(d => d.ID.Equals(c.ID) && d.PRODUCTITEMID.Equals(c.PRODUCTITEMID)))
                        {
                            Product fetchedProd = Apps.dbContext.Product.Where(d => d.ID.Equals(c.PRODUCTITEMID)).FirstOrDefault();
                            SimilarProduct similarProd = new SimilarProduct(fetchedProd);
                            similarProd.ID = Apps.dbContext.SimilarProduct.ToList().Count > 0 ? (Apps.dbContext.SimilarProduct.Max(d => d.ID) + 1) : 1;
                            similarProd.VERSION = 1;

                            mainProd.LSTSIMILARPRODUCTS.Add(similarProd);
                        }
                    });

                    mainProd.LSTSIMILARPRODUCTS.ToList().ForEach(c =>
                    {
                        if (!prod.LSTSIMILARPRODUCTS.Any(d => d.NAME.Equals(c.NAME) && d.PRODUCTITEMID.Equals(c.PRODUCTITEMID)))
                        {
                            mainProd.LSTSIMILARPRODUCTS.Remove(c);
                        }
                    });
                    Apps.dbContext.SaveChanges();
                    #endregion
                    result = Messages.Succcess;
                }
                else
                {
                    result = Messages.ProductMsg + " " + Messages.DoesnotExists;
                }
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while AddOrUpdateSimilarProduct: " + ex.Message, ex);
                result = ex.Message;
            }
            Apps.Logger.Info("Result: " + result);
            return result;
        }

        internal static string UpdateSimilarProduct(Product prod)
        {
            string result = string.Empty;
            Apps.Logger.Info("Updating Similar Products After product update.");

            try
            {
                if (Apps.dbContext.SimilarProduct.Any(c => c.PRODUCTITEMID.Equals(prod.ID)))
                {
                    Apps.dbContext.SimilarProduct.Where(c => c.PRODUCTITEMID.Equals(prod.ID)).ToList().ForEach(c =>
                    {
                        c.IMAGELOCATION = prod.IMAGELOCATION;
                        c.IMAGENAME = prod.IMAGENAME;
                        c.WEBIMAGELOCATION = prod.WEBIMAGELOCATION;
                        c.TEMPIMAGELOCATION = prod.TEMPIMAGELOCATION;
                        c.NAME = prod.NAME;
                        c.ITEMCODE = prod.ITEMCODE;
                        c.VERSION += 1;
                        c.LASTUPDATEDTIME = DateTime.Now;
                        Apps.dbContext.SaveChanges();
                    });
                    result = Messages.Succcess;
                }
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while UpdateSimilarProduct: " + ex.Message, ex);
                result = ex.Message;
            }
            return result;
        }
    }
}
