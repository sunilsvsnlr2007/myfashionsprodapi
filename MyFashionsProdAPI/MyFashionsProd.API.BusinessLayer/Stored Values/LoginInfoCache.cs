﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer
{
    public class LoginInfoCache
    {
        public static long CompanyID { get; set; }
        public static long StoreID { get; set; }
        public static string CompanyName { get; set; }
        public static string StoreName { get; set; }
        public static LoginType LoginType { get; set; }
    }
}
