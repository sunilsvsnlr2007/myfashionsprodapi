﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.ClientSDK.Super_Admin_Calls
{
    public class SuperAdminCalls
    {
        public string ValidateSuperAdmin()
        {
            string username = "ADMIN";
            string password = "MYFASHIONS";
            string frameURI = "http://localhost/MyFashionsProdAPI/";
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            Uri baseAddress = new Uri(frameURI);
            client.BaseAddress = baseAddress;
            string uri = "api/SuperAdmin/ValidateSuperAdmin?username=" + username + "&password=" + password;
            HttpResponseMessage response = client.GetAsync(uri).Result;
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return "Success";
            }
            return "Fail";
        }
    }
}
