﻿using Microsoft.Web.Administration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace MyFashionsProdAPI.CustomActions.WEBAPI
{
    [RunInstaller(true)]
    public partial class CustomInstaller : System.Configuration.Install.Installer
    {
        CustomActionLog _activityLog = new CustomActionLog();
        string iisPath = string.Empty;
        Hashtable htPreLoadedWebConfig;
        string previousWebConfigText = string.Empty;
        public CustomInstaller()
        {
            InitializeComponent();
            this.BeforeInstall += CustomInstaller_BeforeInstall;
        }

        void CustomInstaller_BeforeInstall(object sender, InstallEventArgs e)
        {
            htPreLoadedWebConfig = new Hashtable();
            previousWebConfigText = string.Empty;

            _activityLog.WriteEntry("New MyFashionsProdAPI Service - Before Install Event Started.");

            string uiLevel = Context.Parameters["ui"];
            _activityLog.WriteEntry("Before Start UI Level: " + uiLevel);
            string targetSite = Context.Parameters["targetsite"];
            string targetVDir = "MyFashionsProdAPI";
            string targetAppPool = "MyFashionsProdAPI";

            _activityLog.WriteEntry("New MyFashionsProdAPI Service - Before Insall Event - TargetSite: " + targetSite + " TargetVDIR: " + targetVDir + " TargetAppPool: " + targetAppPool);

            if (targetSite == null)
            {
                _activityLog.WriteEntry("New MyFashionsProdAPI Service - Before Insall Event - IIS Site Name Not Specified.");
                throw new InstallException("IIS Site Name Not Specified.");
            }

            using (ServerManager serverManager = new ServerManager())
            {
                _activityLog.WriteEntry("New MyFashionsProdAPI Service - Before Insall Event - Server Manager Object Created.");
                if (serverManager.ApplicationPools[targetAppPool] == null)
                {
                    _activityLog.WriteEntry("New MyFashionsProdAPI Service - Before Insall Event - Creating New Application Pool: " + targetAppPool);
                    ApplicationPool newPool = serverManager.ApplicationPools.Add(targetAppPool);
                    newPool.ManagedRuntimeVersion = "v4.0";
                    newPool.Enable32BitAppOnWin64 = true;
                    newPool.ManagedPipelineMode = ManagedPipelineMode.Integrated;
                    newPool.ProcessModel.IdentityType = ProcessModelIdentityType.NetworkService;
                    newPool.ProcessModel.LoadUserProfile = true;
                    serverManager.CommitChanges();
                    _activityLog.WriteEntry("New MyFashionsProdAPI Service - Before Insall App Pool " + targetAppPool + " Created Successfully.");
                }
                else
                {
                    _activityLog.WriteEntry("New MyFashionsProdAPI Service - Before Insall Application Pool already exists.");
                }
            }

            _activityLog.WriteEntry("New MyFashionsProdAPI Service - Before Insall Event Ended.");
        }

        public override void Install(IDictionary stateSaver)
        {
            _activityLog.WriteEntry("New MyFashionsProdAPI Service - Installer Calling base Installer.");
            base.Install(stateSaver);
        }
    }
}
