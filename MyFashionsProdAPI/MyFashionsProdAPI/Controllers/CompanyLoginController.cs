﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.Authentication;
using MyFashionsProdAPI.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyFashionsProdAPI.Controllers
{
    [AuthenticationFilter]
    public class CompanyLoginController : ApiController
    {
        [ActionName("ValidateCompanyLogin")]
        [HttpGet]
        public HttpResponseMessage ValidateCompanyLogin(string username, string password)
        {
            if (((int)LoginInfoCache.LoginType) <= 1)
            {
                Apps.Logger.Info("Initiated CompanyLoginController -> ValidateCompanyLogin. UserName: " + username + " Password: " + password);
                long companyID = 0;
                CompanyOperationsLogic companyOperObj = new CompanyOperationsLogic();
                string result = companyOperObj.ValidateCompanyLogin(username, password, out companyID);
                if (result.Equals(Messages.Succcess))
                {
                    return Request.CreateResponse<long>(HttpStatusCode.OK, companyID);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, new HttpError(Messages.PermissionDenied));
            }
        }

        [ActionName("ChangePasword")]
        [HttpGet]
        public HttpResponseMessage ChangePasword(string username, string oldpassword, string newpassword)
        {
            if (((int)LoginInfoCache.LoginType) <= 1)
            {
                Apps.Logger.Info("Initiated CompanyLoginController -> ChangePasword. UserName: " + username + " OldPassword: " + oldpassword + " NewPassword: " + newpassword);
                long companyID = 0;
                CompanyOperationsLogic companyOperObj = new CompanyOperationsLogic();
                string result = companyOperObj.ValidateCompanyLogin(username, oldpassword, out companyID);
                if (result.Equals(Messages.Succcess))
                {
                    if (companyOperObj.ChangePassword(username, oldpassword, newpassword))
                        return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
                    else
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(Messages.FailedUpdatingPassword));
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, new HttpError(Messages.PermissionDenied));
            }

        }

        [ActionName("GetAllCompanies")]
        [HttpGet]
        public HttpResponseMessage GetAllCompanies(long superadminid)
        {
            if (((int)LoginInfoCache.LoginType) <= 1)
            {
                Apps.Logger.Info("Initiated CompanyLoginController -> GetAllCompanies. SuperAdminID: " + superadminid);
                CompanyOperationsLogic companyOperObj = new CompanyOperationsLogic();
                List<Company> lstSuperAdmins = companyOperObj.GetAllCompanies(superadminid);
                return Request.CreateResponse<List<Company>>(HttpStatusCode.OK, lstSuperAdmins);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, new HttpError(Messages.PermissionDenied));
            }
        }

        [ActionName("GetCompanyInfo")]
        [HttpGet]
        public HttpResponseMessage GetCompanyInfo(long companyID)
        {
            if (((int)LoginInfoCache.LoginType) <= 1)
            {
                Apps.Logger.Info("Initiated CompanyLoginController -> GetCompanyInfo. CompanyID: " + companyID);
                string errorMsg = string.Empty;
                CompanyOperationsLogic companyOperObj = new CompanyOperationsLogic();
                Company companyInfo = companyOperObj.GetCompanyInfo(companyID, out errorMsg);
                if (string.IsNullOrEmpty(errorMsg))
                {
                    return Request.CreateResponse<Company>(HttpStatusCode.OK, companyInfo);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, new HttpError(Messages.PermissionDenied));
            }
        }

        [ActionName("CompanyCRUDOperation")]
        [HttpPost]
        public HttpResponseMessage CompanyCRUDOperation(Company companyobj, string parameter)
        {
            if (((int)LoginInfoCache.LoginType) <= 1)
            {
                string serializedObject = Newtonsoft.Json.JsonConvert.SerializeObject(companyobj);
                Apps.Logger.Info("Initiated CompanyLoginController -> CompanyCRUDOperation.");
                Apps.Logger.Info("Company Object: " + serializedObject);
                Apps.Logger.Info("Parameter: " + parameter);
                CompanyOperationsLogic companyOperObj = new CompanyOperationsLogic();
                string result = Messages.InvalidCRUDOperation;
                switch (parameter)
                {
                    case "C":
                        result = companyOperObj.AddCompany(companyobj);
                        break;
                    case "U":
                        result = companyOperObj.UpdateCompany(companyobj);
                        break;
                    case "D":
                        result = companyOperObj.DeleteCompany(companyobj);
                        break;
                    default:
                        break;
                }

                if (result.Equals(Messages.Succcess))
                {
                    return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, new HttpError(Messages.PermissionDenied));
            }
        }
    }
}