﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.Market_Price;
using MyFashionsProdAPI.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyFashionsProdAPI.Controllers
{
    [AuthenticationFilter]
    public class MarketPriceController : ApiController
    {
        [ActionName("GetMarketPriceUsingFilterID")]
        [HttpGet]
        public HttpResponseMessage GetMarketPriceUsingFilterID(long filterID)
        {
            Apps.Logger.Info("Initiated MarketPriceController -> GetMarketPriceUsingFilterID. Filter ID: " + filterID);
            string errorMsg = string.Empty;
            MarketPriceOperationsLogic marketPriceOper = new MarketPriceOperationsLogic();
            MarketPrice marketPrice = marketPriceOper.GetMarketPriceUsingFilterID(filterID, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<MarketPrice>(HttpStatusCode.OK, marketPrice);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }

        [ActionName("MarketPriceCRUDOperation")]
        [HttpPost]
        public HttpResponseMessage MarketPriceCRUDOperation(MasterFilters masterFilter, string parameter)
        {
            string serializedObject = Newtonsoft.Json.JsonConvert.SerializeObject(masterFilter);
            Apps.Logger.Info("Initiated MarketPriceController -> MarketPriceCRUDOperation.");
            Apps.Logger.Info("MasterFilter Object: " + serializedObject);
            Apps.Logger.Info("Parameter: " + parameter);
            string result = Messages.InvalidCRUDOperation;
            MarketPriceOperationsLogic marketPriceOper = new MarketPriceOperationsLogic();
            switch (parameter)
            {
                case "C":
                    result = marketPriceOper.AddMarketPrice(masterFilter);
                    break;
                case "U":
                    result = marketPriceOper.UpdateMarketPrice(masterFilter);
                    break;
                case "D":
                    result = marketPriceOper.DeleteMarketPrice(masterFilter);
                    break;
                default:
                    break;
            }

            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }
    }
}
