﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.Categories;
using MyFashionsProdAPI.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyFashionsProdAPI.Controllers
{
    [AuthenticationFilter]
    public class MasterCategoriesController : ApiController
    {
        [ActionName("GetAllCategories")]
        [HttpGet]
        public HttpResponseMessage GetAllCategories(long relatedcompanyid)
        {
            Apps.Logger.Info("Initiated MasterCategoriesController -> GetAllCategories. Related Company ID: " + relatedcompanyid);
            string errorMsg = string.Empty;
            MasterCategoriesOperationsLogic masterCategOperObj = new MasterCategoriesOperationsLogic();
            MainEnum mainMasterCategories = masterCategOperObj.GetAllMainCategories(relatedcompanyid, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
            {
                return Request.CreateResponse<MainEnum>(HttpStatusCode.OK, mainMasterCategories);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
            }
        }

        [ActionName("GetSubCategories")]
        [HttpGet]
        public HttpResponseMessage GetSubCategories(long relatedmastercategoryid)
        {
            Apps.Logger.Info("Initiated MasterCategoriesController -> GetSubCategories. Related Master Category ID: " + relatedmastercategoryid);
            string errorMsg = string.Empty;
            MasterCategoriesOperationsLogic masterCategOperObj = new MasterCategoriesOperationsLogic();
            List<MasterCategories> lstSubCategories = masterCategOperObj.GetSubCategories(relatedmastercategoryid, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<List<MasterCategories>>(HttpStatusCode.OK, lstSubCategories);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }

        [ActionName("GetSingleCategory")]
        [HttpGet]
        public HttpResponseMessage GetSingleCategory(long relatedcategoryid)
        {
            Apps.Logger.Info("Initiated MasterCategoriesController -> GetSingleCategory. Related Category ID: " + relatedcategoryid);
            string errorMsg = string.Empty;
            MasterCategoriesOperationsLogic masterCategOperObj = new MasterCategoriesOperationsLogic();
            MasterCategories relatedCategory = masterCategOperObj.GetSingleCategory(relatedcategoryid, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<MasterCategories>(HttpStatusCode.OK, relatedCategory);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }

        [ActionName("MasterCategoriesCRUDOperation")]
        [HttpPost]
        public HttpResponseMessage MasterCategoriesCRUDOperation(MasterCategories masterCateg, string parameter)
        {
            if (((int)LoginInfoCache.LoginType) <= 1)
            {
                string serializedObject = Newtonsoft.Json.JsonConvert.SerializeObject(masterCateg);
                Apps.Logger.Info("Initiated MasterCategoriesController -> MasterCategoriesCRUDOperation.");
                Apps.Logger.Info("MasterCategory Object: " + serializedObject);
                Apps.Logger.Info("Parameter: " + parameter);
                string result = Messages.InvalidCRUDOperation;
                MasterCategoriesOperationsLogic masterOperations = new MasterCategoriesOperationsLogic();
                switch (parameter)
                {
                    case "C":
                        result = masterOperations.AddMasterCategory(masterCateg);
                        break;
                    case "U":
                        result = masterOperations.UpdateMasterCategory(masterCateg);
                        break;
                    case "D":
                        result = masterOperations.DeleteMasterCategory(masterCateg);
                        break;
                    default:
                        break;
                }

                if (result.Equals(Messages.Succcess))
                {
                    return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, new HttpError(Messages.PermissionDenied));
            }
        }
    }
}
