﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.Filters;
using MyFashionsProdAPI.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyFashionsProdAPI.Controllers
{
    [AuthenticationFilter]
    public class MasterFiltersController : ApiController
    {
        [ActionName("GetAllFiltersUnderCompany")]
        [HttpGet]
        public HttpResponseMessage GetAllFiltersUnderCompany(long relatedcompanyid)
        {
            Apps.Logger.Info("Initiated MasterFiltersController -> GetAllFiltersUnderCompany. Related Company ID: " + relatedcompanyid);
            string errorMsg = string.Empty;
            MasterFiltersOperationsLogic masterFilterLogic = new MasterFiltersOperationsLogic();
            List<MasterFilters> lstMasterFilters = masterFilterLogic.GetAllFiltersUnderCompany(relatedcompanyid, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<List<MasterFilters>>(HttpStatusCode.OK, lstMasterFilters);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }

        [ActionName("GetFiltersUnderStore")]
        [HttpGet]
        public HttpResponseMessage GetFiltersUnderStore(long relatedStoreID)
        {
            Apps.Logger.Info("Initiated MasterFiltersController -> GetFiltersUnderStore. Related Store ID: " + relatedStoreID);
            string errorMsg = string.Empty;
            MasterFiltersOperationsLogic masterFilterLogic = new MasterFiltersOperationsLogic();
            List<MasterFilters> lstMasterFilters = masterFilterLogic.GetFiltersUnderStore(relatedStoreID, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<List<MasterFilters>>(HttpStatusCode.OK, lstMasterFilters);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }

        [ActionName("GetFilterByItsFilterID")]
        [HttpGet]
        public HttpResponseMessage GetFilterByItsFilterID(long filterID)
        {
            Apps.Logger.Info("Initiated MasterFiltersController -> GetFilterByItsFilterID. Filter ID: " + filterID);
            string errorMsg = string.Empty;
            MasterFiltersOperationsLogic masterFilterLogic = new MasterFiltersOperationsLogic();
            MasterFilters fetchedFilter = masterFilterLogic.GetFilterByItsFilterID(filterID, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<MasterFilters>(HttpStatusCode.OK, fetchedFilter);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }

        [ActionName("MasterFiltersCRUDOperation")]
        [HttpPost]
        public HttpResponseMessage MasterFiltersCRUDOperation(MasterFilters masterFilter, string parameter)
        {
            string serializedObject = Newtonsoft.Json.JsonConvert.SerializeObject(masterFilter);
            Apps.Logger.Info("Initiated MasterFiltersController -> MasterFiltersCRUDOperation.");
            Apps.Logger.Info("Master Filter Object: " + serializedObject);
            Apps.Logger.Info("Paramter: " + parameter);
            string result = Messages.InvalidCRUDOperation;
            MasterFiltersOperationsLogic masterFilterOperations = new MasterFiltersOperationsLogic();
            switch (parameter)
            {
                case "C":
                    result = masterFilterOperations.AddFilter(masterFilter);
                    break;
                case "U":
                    result = masterFilterOperations.UpdateFilter(masterFilter);
                    break;
                case "D":
                    result = masterFilterOperations.DeleteFilter(masterFilter);
                    break;
                default:
                    break;
            }

            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }
    }
}