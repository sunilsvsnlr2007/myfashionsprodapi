﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.Product_Logic;
using MyFashionsProdAPI.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyFashionsProdAPI.Controllers
{
    [AuthenticationFilter]
    public class MasterProductsController : ApiController
    {
        [ActionName("GetAllProductsUnderStore")]
        [HttpGet]
        public HttpResponseMessage GetAllProductsUnderStore(long storeID)
        {
            Apps.Logger.Info("Initiated MasterProductsController -> GetAllProductsUnderStore. StoreID: " + storeID);
            string result = string.Empty;
            ProductsOperationLogic prodOperObj = new ProductsOperationLogic();
            List<Product> lstProducts = prodOperObj.GetAllProductsUnderStore(storeID, out result);
            if(result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<List<Product>>(HttpStatusCode.OK, lstProducts);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }

        [ActionName("MasterProductCRUDOperation")]
        [HttpPost]
        public HttpResponseMessage MasterProductCRUDOperation(Product masterProd, string parameter)
        {
            string serializedObject = Newtonsoft.Json.JsonConvert.SerializeObject(masterProd);
            Apps.Logger.Info("Initiated MasterProductsController -> MasterProductCRUDOperation.");
            Apps.Logger.Info("Product Object: " + serializedObject);
            Apps.Logger.Info("Paramter: " + parameter);
            string result = Messages.InvalidCRUDOperation;
            ProductsOperationLogic productOperObj = new ProductsOperationLogic();
            switch (parameter)
            {
                case "C":
                    result = productOperObj.AddProduct(masterProd);
                    break;
                case "U":
                    result = productOperObj.UpdateProduct(masterProd);
                    break;
                case "D":
                    result = productOperObj.DeleteProduct(masterProd);
                    break;
                default:
                    break;
            }

            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }
    }
}
