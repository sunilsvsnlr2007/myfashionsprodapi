﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.Similar_Products_Logic;
using MyFashionsProdAPI.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyFashionsProdAPI.Controllers
{
    [AuthenticationFilter]
    public class SimilarProductController : ApiController
    {
        [ActionName("GetAllSimilarProductUnderProduct")]
        [HttpGet]
        public HttpResponseMessage GetAllSimilarProductUnderProduct(long productID)
        {
            Apps.Logger.Info("Initiated SimilarProductController -> GetAllSimilarProductUnderProduct. Product ID: " + productID);
            string result = string.Empty;
            SimilarProductOperationsLogic similarProdOperObj = new SimilarProductOperationsLogic();
            List<SimilarProduct> lstSimilarProds = similarProdOperObj.GetAllSimilarProductUnderProduct(productID, out result);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<List<SimilarProduct>>(HttpStatusCode.OK, lstSimilarProds);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }

        [ActionName("AddOrupdateSimilarProduct")]
        [HttpPost]
        public HttpResponseMessage AddOrupdateSimilarProduct(Product prod)
        {
            string serializedObject = Newtonsoft.Json.JsonConvert.SerializeObject(prod);
            Apps.Logger.Info("Initiated SimilarProductController -> AddOrupdateSimilarProduct.");
            Apps.Logger.Info("Product Object: " + serializedObject);
            string result = string.Empty;
            SimilarProductOperationsLogic similarProdOperObj = new SimilarProductOperationsLogic();
            result = similarProdOperObj.AddOrupdateSimilarProduct(prod);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }
    }
}
