﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.Authentication;
using MyFashionsProdAPI.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyFashionsProdAPI.Controllers
{
    [AuthenticationFilter]
    public class StoreLoginController : ApiController
    {
        [ActionName("ValidateStoreLogin")]
        [HttpGet]
        public HttpResponseMessage ValidateStoreLogin(string username, string password)
        {
            Apps.Logger.Info("Initiated StoreLoginController -> ValidateStoreLogin. UserName: " + username + " Password: " + password);
            username = username.ToUpper();
            password = password.ToUpper();
            long storeID = 0;
            StoreOperationsLogic storeOperObj = new StoreOperationsLogic();
            string result = storeOperObj.ValidateStoreLogin(username, password, out storeID);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<long>(HttpStatusCode.OK, storeID);
            }
            else
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, result);
            }
        }

        [ActionName("ChangePasword")]
        [HttpGet]
        public HttpResponseMessage ChangePasword(string username, string oldpassword, string newpassword)
        {
            Apps.Logger.Info("Initiated StoreLoginController -> ChangePasword. UserName: " + username + " OldPassword: " + oldpassword + " NewPassword: " + newpassword);
            username = username.ToUpper();
            oldpassword = oldpassword.ToUpper();
            newpassword = newpassword.ToUpper();
            long storeID = 0;
            StoreOperationsLogic storeOperObj = new StoreOperationsLogic();
            string result = storeOperObj.ValidateStoreLogin(username, oldpassword, out storeID);
            if (result.Equals(Messages.Succcess))
            {
                if (storeOperObj.ChangePassword(username, oldpassword, newpassword))
                    return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
                else
                    return Request.CreateResponse<string>(HttpStatusCode.BadRequest, Messages.FailedUpdatingPassword);
            }
            else
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, result);
        }

        [ActionName("GetAllStores")]
        [HttpGet]
        public HttpResponseMessage GetAllStores(long relatedcompanyid)
        {
            Apps.Logger.Info("Initiated StoreLoginController -> GetAllStores. Related Company ID: " + relatedcompanyid);
            StoreOperationsLogic storeOperObj = new StoreOperationsLogic();
            List<Stores> lstStores = storeOperObj.GetAllStores(relatedcompanyid);
            return Request.CreateResponse<List<Stores>>(HttpStatusCode.OK, lstStores);
        }

        [ActionName("GetStoreInfo")]
        [HttpGet]
        public HttpResponseMessage GetStoreInfo(long relatedstoreid)
        {
            Apps.Logger.Info("Initiated StoreLoginController -> GetStoreInfo. Related Store ID: " + relatedstoreid);
            StoreOperationsLogic storeOperObj = new StoreOperationsLogic();
            string errorMsg = string.Empty;
            Stores storeInfo = storeOperObj.GetStoreInfo(relatedstoreid, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<Stores>(HttpStatusCode.OK, storeInfo);
            else
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, errorMsg);
        }

        [ActionName("GetCompanyInfoByStoreID")]
        [HttpGet]
        public HttpResponseMessage GetCompanyInfoByStoreID(long storeid)
        {
            Apps.Logger.Info("Initiated StoreLoginController -> GetCompanyInfoByStoreID. Store ID: " + storeid);
            StoreOperationsLogic storeOperObj = new StoreOperationsLogic();
            string errorMsg = string.Empty;
            Company company = storeOperObj.GetCompanyInfoByStoreID(storeid, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<Company>(HttpStatusCode.OK, company);
            else
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, errorMsg);
        }

        [ActionName("StoreCRUDOperation")]
        [HttpPost]
        public HttpResponseMessage StoreCRUDOperation(Stores storeObj, string parameter)
        {
            string serializedObject = Newtonsoft.Json.JsonConvert.SerializeObject(storeObj);
            Apps.Logger.Info("Initiated StoreLoginController -> StoreCRUDOperation.");
            Apps.Logger.Info("Store Object: " + serializedObject);
            Apps.Logger.Info("Parameter: " + parameter);
            StoreOperationsLogic storeOperObj = new StoreOperationsLogic();
            string result = Messages.InvalidCRUDOperation;
            switch (parameter)
            {
                case "C":
                    result = storeOperObj.AddStore(storeObj);
                    break;
                case "U":
                    result = storeOperObj.UpdateStore(storeObj);
                    break;
                case "D":
                    result = storeOperObj.DeleteStore(storeObj);
                    break;
                default:
                    break;
            }
            
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            else
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, result);
            }
        }
    }
}
