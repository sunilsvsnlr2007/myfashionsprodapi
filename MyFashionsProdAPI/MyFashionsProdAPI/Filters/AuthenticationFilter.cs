﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;

namespace MyFashionsProdAPI.Filters
{
    public class AuthenticationFilter : BasicAuthenticationFilter
    {
        public AuthenticationFilter()
        { }

        public AuthenticationFilter(bool active)
            : base(active)
        { }

        protected override bool OnAuthorizeUser(string username, string password, string adminType, HttpActionContext actionContext)
        {
            string errorMsg = string.Empty;
            string result = string.Empty;
            switch (adminType)
            {
                case "A"://A - Super Admin Login
                    long superAdminID = 0;
                    SuperAdminLogic superAdmin = new SuperAdminLogic();
                    result = superAdmin.ValidateSuperAdmin(username, password, out superAdminID);
                    if (result.Equals(Messages.Succcess))
                    {
                        LoginInfoCache.LoginType = LoginType.SuperAdmin;
                        return true;
                    }
                    break;
                case "C"://C - Company Login
                    long companyID = 0;
                    CompanyOperationsLogic companyOper = new CompanyOperationsLogic();
                    result = companyOper.ValidateCompanyLogin(username, password, out companyID);
                    if (result.Equals(Messages.Succcess))
                    {
                        Company companyInfo = companyOper.GetCompanyInfo(companyID, out errorMsg);
                        if (string.IsNullOrEmpty(errorMsg))
                        {
                            LoginInfoCache.CompanyID = companyInfo.ID;
                            LoginInfoCache.CompanyName = companyInfo.NAME;
                            LoginInfoCache.LoginType = LoginType.Company;
                            return true;
                        }
                    }
                    break;
                case "S"://S - Store Login
                    long storeID = 0;
                    StoreOperationsLogic storeOper = new StoreOperationsLogic();
                    result = storeOper.ValidateStoreLogin(username, password, out storeID);
                    if (result.Equals(Messages.Succcess))
                    {
                        Stores storeInfo = storeOper.GetStoreInfo(storeID, out errorMsg);
                        if (string.IsNullOrEmpty(errorMsg))
                        {
                            LoginInfoCache.CompanyID = storeInfo.REGISTEREDCOMPANY.ID;
                            LoginInfoCache.CompanyName = storeInfo.REGISTEREDCOMPANY.NAME;
                            LoginInfoCache.LoginType = LoginType.Store;
                            LoginInfoCache.StoreID = storeInfo.ID;
                            LoginInfoCache.StoreName = storeInfo.NAME;
                            return true;
                        }
                    }
                    break;
                default:
                    return false;
            }
            return false;
        }
    }
}