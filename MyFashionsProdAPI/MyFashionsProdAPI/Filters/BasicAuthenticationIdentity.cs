﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace MyFashionsProdAPI.Filters
{
    public class BasicAuthenticationIdentity : GenericIdentity
    {
        public BasicAuthenticationIdentity(string name, string password,string adminType)
            : base(name, "Basic")
        {
            this.Password = password;
            this.AdminType = adminType;
        }

        /// <summary>
        /// Basic Auth Password for custom authentication
        /// </summary>
        public string Password { get; set; }
        public string AdminType { get; set; }
    }
}